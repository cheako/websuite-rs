pub mod normal {
    use std::collections::HashMap;
    use std::ops::Deref;
    use std::sync::{Arc, RwLock};

    pub trait Key: Eq + std::hash::Hash + Clone {}
    impl<T: Eq + std::hash::Hash + Clone> Key for T {}

    pub trait Value: Default {
        fn is_empty(&mut self) -> bool;
    }

    type Shared<K, V> = Arc<RwLock<HashMap<K, V>>>;

    #[derive(Clone, Default, Debug)]
    pub struct Factory<K: Key, V: Value>(Shared<K, V>);
    #[derive(Debug)]
    pub struct Ref<T, K: Key, V: Value>(T, K, Shared<K, V>);

    impl<T, K: Key, V: Value> Deref for Ref<T, K, V> {
        type Target = T;

        fn deref(&self) -> &Self::Target {
            &self.0
        }
    }

    impl<T, K: Key, V: Value> Drop for Ref<T, K, V> {
        fn drop(&mut self) {
            if let std::collections::hash_map::Entry::Occupied(mut v) =
                self.2.write().unwrap().entry(self.1.clone())
            {
                if v.get_mut().is_empty() {
                    v.remove_entry();
                }
            }
        }
    }

    impl<K: Key, V: Value> Factory<K, V> {
        pub fn map<T, F: FnMut(&mut V) -> T>(&self, key: K, f: &mut F) -> Ref<T, K, V> {
            let k = key.clone();
            Ref(
                f(self.0.write().unwrap().entry(key).or_default()),
                k,
                self.0.clone(),
            )
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
