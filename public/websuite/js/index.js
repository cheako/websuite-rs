"use strict";

import { bytesToBase64 } from "./base64.js";

export function dom_rendered() {
  // mnemonics is populated as required by getLanguage
  var mnemonics = { english: new Mnemonic("english") };
  var mnemonic = mnemonics["english"];
  var phrase = "";
  var passphrase = "";
  var seed = null;
  var bip32RootKey = null;
  var network = {
    messagePrefix: "\u0018Web Suite Signed Message:\n",
    bip32: {
      public: 76067358 - 13,
      private: 76066276 - 13,
    },
    pubKeyHash: 13,
    scriptHash: 5 + 13,
    wif: 128,
  };

  var showQr = false;

  var phraseChangeTimeoutEvent = null;
  var seedChangedTimeoutEvent = null;
  var rootKeyChangedTimeoutEvent = null;

  var DOM = {};
  DOM.phrase = $("#phrase");
  DOM.phraseSplit = $("#phraseSplit");
  DOM.phraseSplitWarn = $("#phraseSplitWarn");
  DOM.passphrase = $("#passphrase");
  DOM.generate = $("#generate");
  DOM.seed = $("#seed");
  DOM.rootKey = $("#root-key");
  DOM.generatedStrength = $("#strength");
  DOM.generatedStrengthWarning = $("#generate-container-warning");
  DOM.languages = $("#languages a");
  DOM.feedback = $("#feedback");
  DOM.qrContainer = $("#qr-container");
  DOM.qrHider = $("#qr-hider");
  DOM.qrImage = $("#qr-image");
  DOM.qrHint = $(".qr-hint");
  DOM.showQrEls = $("[data-show-qr]");
  DOM.d = $("#d");
  DOM.chainCode = $("#chain-code");

  function init() {
    // Events
    DOM.generatedStrength.on("change", generatedStrengthChanged);
    DOM.phrase.on("change", delayedPhraseChanged);
    DOM.passphrase.on("change", delayedPhraseChanged);
    DOM.generate.on("click", generateClicked);
    DOM.seed.on("change", delayedSeedChanged);
    DOM.rootKey.on("change", delayedRootKeyChanged);
    DOM.languages.on("click", languageChanged);
    setQrEvents(DOM.showQrEls);
    disableForms();
    hidePending();
    hideValidationError();
  }

  // Event handlers

  function generatedStrengthChanged() {
    var strength = parseInt(DOM.generatedStrength.val());
    if (strength < 12) {
      DOM.generatedStrengthWarning.removeClass("hidden");
    } else {
      DOM.generatedStrengthWarning.addClass("hidden");
    }
  }

  function delayedPhraseChanged() {
    showPending();
    hideValidationError();
    seed = null;
    bip32RootKey = null;
    if (phraseChangeTimeoutEvent != null) {
      clearTimeout(phraseChangeTimeoutEvent);
    }
    phraseChangeTimeoutEvent = setTimeout(function () {
      phraseChanged();
    }, 400);
  }

  function phraseChanged() {
    // Get the mnemonic phrase
    phrase = DOM.phrase.val();
    var errorText = findPhraseErrors(phrase);
    if (errorText) {
      showValidationError(errorText);
      return;
    }
    // Calculate and display
    writeSplitPhrase(phrase);
    passphrase = DOM.passphrase.val();
    seed = mnemonic.toSeed(phrase, passphrase);
    bip32RootKey = libs.bitcoin.HDNode.fromSeedHex(seed, network);
    DOM.seed.val(seed);
    var rootKey = bip32RootKey.toBase58();
    DOM.rootKey.val(rootKey);
    DOM.d.val(bip32RootKey.keyPair.d.toString(36));
    DOM.chainCode.val(bytesToBase64(bip32RootKey.chainCode));
    hidePending();
  }

  function delayedSeedChanged() {
    hideValidationError();
    showPending();
    // Clear existing mnemonic and passphrase
    DOM.phrase.val("");
    DOM.phraseSplit.val("");
    DOM.passphrase.val("");
    phrase = "";
    passphrase = "";
    if (seedChangedTimeoutEvent != null) {
      clearTimeout(seedChangedTimeoutEvent);
    }
    seedChangedTimeoutEvent = setTimeout(seedChanged, 400);
  }

  function delayedRootKeyChanged() {
    hideValidationError();
    showPending();
    // Clear existing mnemonic and passphrase
    DOM.phrase.val("");
    DOM.phraseSplit.val("");
    DOM.passphrase.val("");
    DOM.seed.val("");
    phrase = "";
    passphrase = "";
    seed = null;
    if (rootKeyChangedTimeoutEvent != null) {
      clearTimeout(rootKeyChangedTimeoutEvent);
    }
    rootKeyChangedTimeoutEvent = setTimeout(rootKeyChanged, 400);
  }

  function seedChanged() {
    seed = DOM.seed.val();
    try {
      bip32RootKey = libs.bitcoin.HDNode.fromSeedHex(seed, network);
      var rootKeyBase58 = bip32RootKey.toBase58();
      DOM.rootKey.val(rootKeyBase58);
      var errorText = validateRootKey(rootKeyBase58);
      if (errorText) {
        showValidationError(errorText);
        return;
      }
      DOM.d.val(bip32RootKey.keyPair.d.toString(36));
      DOM.chainCode.val(bytesToBase64(bip32RootKey.chainCode));
      hidePending();
    } catch (e) {
      showValidationError(e);
    }
  }

  function rootKeyChanged() {
    var rootKeyBase58 = DOM.rootKey.val();
    var errorText = validateRootKey(rootKeyBase58);
    if (errorText) {
      showValidationError(errorText);
      return;
    }
    bip32RootKey = libs.bitcoin.HDNode.fromBase58(rootKeyBase58, network);
    DOM.d.val(bip32RootKey.keyPair.d.toString(36));
    DOM.chainCode.val(bytesToBase64(bip32RootKey.chainCode));
    hidePending();
  }

  function generateClicked() {
    clearDisplay();
    showPending();
    setTimeout(function () {
      setMnemonicLanguage();
      var phrase = generateRandomPhrase();
      if (!phrase) {
        return;
      }
      phraseChanged();
    }, 50);
  }

  function languageChanged() {
    setTimeout(function () {
      setMnemonicLanguage();
      if (DOM.phrase.val().length > 0) {
        var newPhrase = convertPhraseToNewLanguage();
        DOM.phrase.val(newPhrase);
        phraseChanged();
      } else {
        DOM.generate.trigger("click");
      }
    }, 50);
  }

  // Private methods

  function generateRandomPhrase() {
    if (!hasStrongRandom()) {
      var errorText = "This browser does not support strong randomness";
      showValidationError(errorText);
      return;
    }
    // get the amount of entropy to use
    var numWords = parseInt(DOM.generatedStrength.val());
    var strength = (numWords / 3) * 32;
    var buffer = new Uint8Array(strength / 8);
    // create secure entropy
    var data = crypto.getRandomValues(buffer);
    // show the words
    var words = mnemonic.toMnemonic(data);
    DOM.phrase.val(words);
    return words;
  }

  function showValidationError(errorText) {
    DOM.feedback.text(errorText).show();
  }

  function hideValidationError() {
    DOM.feedback.text("").hide();
  }

  function findPhraseErrors(phrase) {
    // Preprocess the words
    phrase = mnemonic.normalizeString(phrase);
    var words = phraseToWordArray(phrase);
    // Detect blank phrase
    if (words.length == 0) {
      return "Blank mnemonic";
    }
    // Check each word
    for (var i = 0; i < words.length; i++) {
      var word = words[i];
      var language = getLanguage();
      if (WORDLISTS[language].indexOf(word) == -1) {
        console.log("Finding closest match to " + word);
        var nearestWord = findNearestWord(word);
        return word + " not in wordlist, did you mean " + nearestWord + "?";
      }
    }
    // Check the words are valid
    var properPhrase = wordArrayToPhrase(words);
    var isValid = mnemonic.check(properPhrase);
    if (!isValid) {
      return "Invalid mnemonic";
    }
    return false;
  }

  function validateRootKey(rootKeyBase58) {
    try {
      libs.bitcoin.HDNode.fromBase58(rootKeyBase58, network);
    } catch (e) {
      return "Invalid root key";
    }
    return "";
  }

  function clearDisplay() {
    DOM.rootKey.val("");
    hideValidationError();
  }

  function hasStrongRandom() {
    return "crypto" in window && window["crypto"] !== null;
  }

  function disableForms() {
    $("form").on("submit", function (e) {
      e.preventDefault();
    });
  }

  function showPending() {
    DOM.feedback.text("Calculating...").show();
  }

  function hidePending() {
    DOM.feedback.text("").hide();
  }

  function findNearestWord(word) {
    var language = getLanguage();
    var words = WORDLISTS[language];
    var minDistance = 99;
    var closestWord = words[0];
    for (var i = 0; i < words.length; i++) {
      var comparedTo = words[i];
      if (comparedTo.indexOf(word) == 0) {
        return comparedTo;
      }
      var distance = libs.levenshtein.get(word, comparedTo);
      if (distance < minDistance) {
        closestWord = comparedTo;
        minDistance = distance;
      }
    }
    return closestWord;
  }

  function getLanguage() {
    var defaultLanguage = "english";
    // Try to get from existing phrase
    var language = getLanguageFromPhrase();
    // Try to get from url if not from phrase
    if (language.length == 0) {
      language = getLanguageFromUrl();
    }
    // Default to English if no other option
    if (language.length == 0) {
      language = defaultLanguage;
    }
    return language;
  }

  function getLanguageFromPhrase(phrase) {
    // Check if how many words from existing phrase match a language.
    var language = "";
    if (!phrase) {
      phrase = DOM.phrase.val();
    }
    if (phrase.length > 0) {
      var words = phraseToWordArray(phrase);
      var languageMatches = {};
      for (var l in WORDLISTS) {
        // Track how many words match in this language
        languageMatches[l] = 0;
        for (var i = 0; i < words.length; i++) {
          var wordInLanguage = WORDLISTS[l].indexOf(words[i]) > -1;
          if (wordInLanguage) {
            languageMatches[l]++;
          }
        }
        // Find languages with most word matches.
        // This is made difficult due to commonalities between Chinese
        // simplified vs traditional.
        var mostMatches = 0;
        var mostMatchedLanguages = [];
        for (var l in languageMatches) {
          var numMatches = languageMatches[l];
          if (numMatches > mostMatches) {
            mostMatches = numMatches;
            mostMatchedLanguages = [l];
          } else if (numMatches == mostMatches) {
            mostMatchedLanguages.push(l);
          }
        }
      }
      if (mostMatchedLanguages.length > 0) {
        // Use first language and warn if multiple detected
        language = mostMatchedLanguages[0];
        if (mostMatchedLanguages.length > 1) {
          console.warn("Multiple possible languages");
          console.warn(mostMatchedLanguages);
        }
      }
    }
    return language;
  }

  function getLanguageFromUrl() {
    for (var language in WORDLISTS) {
      if (window.location.hash.indexOf(language) > -1) {
        return language;
      }
    }
    return "";
  }

  function setMnemonicLanguage() {
    var language = getLanguage();
    // Load the bip39 mnemonic generator for this language if required
    if (!(language in mnemonics)) {
      mnemonics[language] = new Mnemonic(language);
    }
    mnemonic = mnemonics[language];
  }

  function convertPhraseToNewLanguage() {
    var oldLanguage = getLanguageFromPhrase();
    var newLanguage = getLanguageFromUrl();
    var oldPhrase = DOM.phrase.val();
    var oldWords = phraseToWordArray(oldPhrase);
    var newWords = [];
    for (var i = 0; i < oldWords.length; i++) {
      var oldWord = oldWords[i];
      var index = WORDLISTS[oldLanguage].indexOf(oldWord);
      var newWord = WORDLISTS[newLanguage][index];
      newWords.push(newWord);
    }
    newPhrase = wordArrayToPhrase(newWords);
    return newPhrase;
  }

  // TODO look at jsbip39 - mnemonic.splitWords
  function phraseToWordArray(phrase) {
    var words = phrase.split(/\s/g);
    var noBlanks = [];
    for (var i = 0; i < words.length; i++) {
      var word = words[i];
      if (word.length > 0) {
        noBlanks.push(word);
      }
    }
    return noBlanks;
  }

  // TODO look at jsbip39 - mnemonic.joinWords
  function wordArrayToPhrase(words) {
    var phrase = words.join(" ");
    var language = getLanguageFromPhrase(phrase);
    if (language == "japanese") {
      phrase = words.join("\u3000");
    }
    return phrase;
  }

  function writeSplitPhrase(phrase) {
    var wordCount = phrase.split(/\s/g).length;
    var left = [];
    for (var i = 0; i < wordCount; i++) left.push(i);
    var group = [[], [], []],
      groupI = -1;
    var seed = Math.abs(sjcl.hash.sha256.hash(phrase)[0]) % 2147483647;
    while (left.length > 0) {
      groupI = (groupI + 1) % 3;
      seed = (seed * 16807) % 2147483647;
      var selected = Math.floor((left.length * (seed - 1)) / 2147483646);
      group[groupI].push(left[selected]);
      left.splice(selected, 1);
    }
    var cards = [phrase.split(/\s/g), phrase.split(/\s/g), phrase.split(/\s/g)];
    for (var i = 0; i < 3; i++) {
      for (var ii = 0; ii < wordCount / 3; ii++)
        cards[i][group[i][ii]] = "XXXX";
      cards[i] = "Card " + (i + 1) + ": " + wordArrayToPhrase(cards[i]);
    }
    DOM.phraseSplit.val(cards.join("\r\n"));
    var triesPerSecond = 10000000000;
    var hackTime = Math.pow(2, (wordCount * 10) / 3) / triesPerSecond;
    var displayRedText = false;
    if (hackTime < 1) {
      hackTime = "<1 second";
      displayRedText = true;
    } else if (hackTime < 86400) {
      hackTime = Math.floor(hackTime) + " seconds";
      displayRedText = true;
    } else if (hackTime < 31557600) {
      hackTime = Math.floor(hackTime / 86400) + " days";
      displayRedText = true;
    } else {
      hackTime = Math.floor(hackTime / 31557600) + " years";
    }
    DOM.phraseSplitWarn.html("Time to hack with only one card: " + hackTime);
    if (displayRedText) {
      DOM.phraseSplitWarn.addClass("text-danger");
    } else {
      DOM.phraseSplitWarn.removeClass("text-danger");
    }
  }

  function setQrEvents(els) {
    els.on("mouseenter", createQr);
    els.on("mouseleave", destroyQr);
    els.on("click", toggleQr);
  }

  function createQr(e) {
    var content = e.target.textContent || e.target.value;
    if (content) {
      var qrEl = libs.kjua({
        text: content,
        render: "canvas",
        size: 310,
        ecLevel: "H",
      });
      DOM.qrImage.append(qrEl);
      if (!showQr) {
        DOM.qrHider.addClass("hidden");
      } else {
        DOM.qrHider.removeClass("hidden");
      }
      DOM.qrContainer.removeClass("hidden");
    }
  }

  function destroyQr() {
    DOM.qrImage.text("");
    DOM.qrContainer.addClass("hidden");
  }

  function toggleQr() {
    showQr = !showQr;
    DOM.qrHider.toggleClass("hidden");
    DOM.qrHint.toggleClass("hidden");
  }

  init();
}
