#![feature(async_closure)]
use std::cell::RefCell;
use std::convert::TryFrom;
use std::num::NonZeroU32;
use std::rc::Rc;

use concat_idents::concat_idents;
#[allow(unused_imports)]
use js_sys::{
    Array, Int32Array, Object, Promise,
    Reflect::{get, set},
    Uint8Array,
};
pub use jtm;
use jtm::{brainpool_p256r1 as bp, keys::*};
use num_bigint_dig::BigUint;
use serde::{Deserialize, Serialize};
use wasm_bindgen::{JsCast, JsValue};
#[allow(unused_imports)]
use web_sys::{
    Event as WebEvent, IdbDatabase, IdbIndexParameters, IdbKeyRange, IdbObjectStoreParameters,
    IdbOpenDbRequest, IdbTransactionMode,
};

pub use idb::{self, Database, WINDOW};

const DB_NAME: &str = "websuite";
const OS_P256R1: &str = "p256r1";
const OS_HIERARCHY: &str = "hierarchy";
const INDEX_RIPEMD: &str = "ripemd";
const INDEX_RIPEMD_SUM: &str = "ripemd_sum";
const INDEX_HIERARCHY_HISTORY: &str = "hierarchy_history";
const INDEX_HIERARCHY_CHILD: &str = "hierarchy_children";

#[derive(Debug, Clone)]
struct P256R1 {
    d: BigUint,
    h: Point,
    ripemd: bp::RIPEMD160,
    ripemd_sum: bp::RIPEMD160SUM,
}

async fn pubkey_cache(tx: &web_sys::IdbTransaction, d: BigUint) -> Point {
    let os = tx.object_store(OS_P256R1).unwrap();
    let v = idb::get_request(Box::new(os.clone()), JsValue::from_serde(&d).unwrap())
        .await
        .unwrap();
    if v.is_undefined() {
        let h = Point::from(d.clone());
        let ripemd = bp::RIPEMD160::try_from(&h).unwrap();
        let mut ripemd_sum = [0; 4];
        ripemd_sum.copy_from_slice(&ripemd[0..4]);
        let ret = h.clone();
        os.add(
            &JsValue::try_from(P256R1 {
                d,
                h,
                ripemd,
                ripemd_sum,
            })
            .unwrap(),
        )
        .unwrap();
        ret
    } else {
        P256R1::try_from(&v).unwrap().h
    }
}

pub async fn set_h(pkey: &mut PrivKey) {
    match &mut pkey.h {
        Some(_) => {}
        x => {
            let db = get_db().await.unwrap();
            let tx = db
                .transaction_with_str_and_mode(OS_P256R1, IdbTransactionMode::Readwrite)
                .unwrap();
            *x = Some(pubkey_cache(&tx, pkey.d.clone()).await);
        }
    };
}

pub async fn get_privkey_from_ripemd(
    tx: &web_sys::IdbTransaction,
    ripemd: bp::RIPEMD160,
) -> Option<BigUint> {
    let index = tx
        .object_store(OS_P256R1)
        .unwrap()
        .index(INDEX_RIPEMD)
        .unwrap();
    let v = idb::get_request(
        Box::new(index.clone()),
        JsValue::from_serde(&BigUint::from_bytes_le(&ripemd)).unwrap(),
    )
    .await
    .unwrap();
    if v.is_undefined() {
        None
    } else {
        let key = P256R1::try_from(&v).unwrap();
        Some(key.d)
    }
}

pub async fn get_privkey_from_history(
    tx: &web_sys::IdbTransaction,
    privkey: &mut PrivKey,
    history: HierarchyHistory,
) -> Result<PrivKey, PubKey> {
    use std::convert::TryInto;
    let os = tx.object_store(OS_HIERARCHY).unwrap();
    let index = os.index(INDEX_HIERARCHY_HISTORY).unwrap();
    let v = idb::get_request(Box::new(index.clone()), history.clone().try_into().unwrap())
        .await
        .unwrap();
    if v.is_undefined() {
        set_h(privkey).await;
        let mut privkey = privkey.ckd_priv(history.child_number);
        let h = match &mut privkey.h {
            Some(h) => h.clone(),
            x => {
                let h = pubkey_cache(&tx, privkey.d.clone()).await;
                *x = Some(h.clone());
                h
            }
        };
        let ripemd = bp::RIPEMD160::try_from(h.clone()).unwrap();
        let ripemd_sum = bp::RIPEMD160SUM::try_from(h.clone()).unwrap();
        let ret = Hierarchy {
            h,
            chain_code: privkey.chain_code,
            one_left: privkey.one_left,
            ripemd,
            ripemd_sum,
            history,
        };
        os.add(&JsValue::try_from(ret).unwrap()).unwrap();
        Ok(privkey)
    } else {
        let hierarchy = Hierarchy::try_from(&v).unwrap();
        match get_privkey_from_ripemd(tx, hierarchy.ripemd).await {
            Some(d) => Ok(PrivKey {
                d,
                chain_code: hierarchy.chain_code,
                one_left: hierarchy.one_left,
                h: Some(hierarchy.h),
                history: hierarchy.history.into(),
            }),
            None => Err(PubKey {
                h: hierarchy.h,
                chain_code: hierarchy.chain_code,
                one_left: hierarchy.one_left,
                history: hierarchy.history.into(),
            }),
        }
    }
}

pub async fn ckd_priv(pkey: &mut PrivKey, child_number: i32) -> PrivKey {
    use std::convert::TryInto;
    let db = get_db().await.unwrap();
    let tx = db
        .transaction_with_str_sequence_and_mode(
            &Array::of2(&OS_HIERARCHY.into(), &OS_P256R1.into()),
            IdbTransactionMode::Readwrite,
        )
        .unwrap();
    let h = match &mut pkey.h {
        Some(h) => h.clone(),
        x => {
            let h = pubkey_cache(&tx, pkey.d.clone()).await;
            *x = Some(h.clone());
            h
        }
    };
    let history = HierarchyHistory {
        child: HierarchyChild {
            depth: pkey.history.depth + 1,
            parent_fingerprint: h.try_into().unwrap(),
        },
        child_number,
    };
    get_privkey_from_history(&tx, pkey, history).await.unwrap()
}

#[derive(Debug, Copy, Clone)]
pub struct HierarchyChild {
    pub depth: u8,
    pub parent_fingerprint: bp::RIPEMD160SUM,
}

#[derive(Debug, Copy, Clone)]
pub struct HierarchyHistory {
    pub child: HierarchyChild,
    pub child_number: i32,
}

#[derive(Debug, Clone)]
pub struct Hierarchy {
    pub h: Point,
    pub chain_code: jtm::keys::ChainCode,
    pub one_left: Option<jtm::keys::ChainCode>,
    pub ripemd: bp::RIPEMD160,
    pub ripemd_sum: bp::RIPEMD160SUM,
    pub history: HierarchyHistory,
}

fn public_to_child(pkey: &PubKey) -> HierarchyChild {
    HierarchyChild {
        depth: pkey.history.depth,
        parent_fingerprint: pkey.history.parent_fingerprint,
    }
}

fn public_to_history(pkey: &PubKey, child: HierarchyChild) -> HierarchyHistory {
    HierarchyHistory {
        child,
        child_number: pkey.history.child_number,
    }
}

async fn private_to_hierarchy(privkey: &mut PrivKey, history: HierarchyHistory) -> JsValue {
    set_h(privkey).await;
    let h = privkey.get_h().clone();
    let ripemd = bp::RIPEMD160::try_from(&h).unwrap();
    let ripemd_sum = bp::RIPEMD160SUM::try_from(&h).unwrap();
    JsValue::try_from(Hierarchy {
        h,
        chain_code: privkey.chain_code,
        one_left: privkey.one_left,
        ripemd,
        ripemd_sum,
        history,
    })
    .unwrap()
}

pub async fn inster_private_hierarchy(pkey: &mut PrivKey) {
    let db = get_db().await.unwrap();
    #[allow(unused_unsafe)]
    unsafe {
        web_sys::console::log_1(&"4".into());
    }
    let tx = db
        .transaction_with_str_sequence_and_mode(
            &Array::of2(&OS_HIERARCHY.into(), &OS_P256R1.into()),
            IdbTransactionMode::Readwrite,
        )
        .unwrap();
    #[allow(unused_unsafe)]
    unsafe {
        web_sys::console::log_1(&"3".into());
    }
    match &mut pkey.h {
        Some(_) => {}
        x => {
            *x = Some(pubkey_cache(&tx, pkey.d.clone()).await);
        }
    };
    #[allow(unused_unsafe)]
    unsafe {
        web_sys::console::log_1(&"2".into());
    }
    let os_hierarchy = tx.object_store(OS_HIERARCHY).unwrap();
    #[allow(unused_unsafe)]
    unsafe {
        web_sys::console::log_1(&"1".into());
    }
    let pubkey = &pkey.into();
    os_hierarchy
        .add(
            &private_to_hierarchy(pkey, public_to_history(&pubkey, public_to_child(&pubkey))).await,
        )
        .unwrap();
}

// Dragons past here.
#[derive(Serialize, Deserialize, Debug, Copy, Clone)]
struct HierarchyChildStore(/* depth */ u8, /* parent_fingerprint */ u32);
impl From<&HierarchyChildStore> for HierarchyChild {
    fn from(load: &HierarchyChildStore) -> Self {
        Self {
            depth: load.0,
            parent_fingerprint: load.1.to_be_bytes(),
        }
    }
}
impl From<&HierarchyChild> for HierarchyChildStore {
    fn from(store: &HierarchyChild) -> Self {
        Self(store.depth, u32::from_be_bytes(store.parent_fingerprint))
    }
}

#[derive(Serialize, Deserialize, Debug, Copy, Clone)]
struct HierarchyHistoryStore(
    /* depth */ u8,
    /* parent_fingerprint */ u32,
    /* child_number */ i32,
);
impl From<&HierarchyHistoryStore> for HierarchyHistory {
    fn from(load: &HierarchyHistoryStore) -> Self {
        Self {
            child: HierarchyChild {
                depth: load.0,
                parent_fingerprint: load.1.to_be_bytes(),
            },
            child_number: load.2,
        }
    }
}
impl From<&HierarchyHistory> for HierarchyHistoryStore {
    fn from(store: &HierarchyHistory) -> Self {
        Self(
            store.child.depth,
            u32::from_be_bytes(store.child.parent_fingerprint),
            store.child_number,
        )
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct HierarchyStore {
    h: BigUint,
    chain_code: BigUint,
    one_left: Option<BigUint>,
    key: BigUint,
    ripemd_sum: u32,
    history: HierarchyHistoryStore,
    history_child: HierarchyChildStore,
}
impl From<&HierarchyStore> for Hierarchy {
    fn from(load: &HierarchyStore) -> Self {
        let mut point = [0; 33];
        let bytes = load.h.to_bytes_le();
        point[0..bytes.len()].copy_from_slice(&bytes);
        let mut chain_code = [0; 32];
        let bytes = load.chain_code.to_bytes_le();
        chain_code[0..bytes.len()].copy_from_slice(&bytes);
        let mut ret = Self {
            h: point.into(),
            chain_code,
            one_left: load.one_left.as_ref().map(|x| {
                let mut ret = [0; 32];
                let bytes = x.to_bytes_le();
                ret[0..bytes.len()].copy_from_slice(&bytes);
                ret
            }),
            ripemd: Default::default(),
            ripemd_sum: load.ripemd_sum.to_be_bytes(),
            history: load.history.into(),
        };
        let bytes = load.key.to_bytes_le();
        ret.ripemd[0..bytes.len()].copy_from_slice(&bytes);
        ret
    }
}
impl From<&Hierarchy> for HierarchyStore {
    fn from(store: &Hierarchy) -> Self {
        Self {
            h: BigUint::from_bytes_le(&bp::CompressedPoint::try_from(&store.h).unwrap()),
            chain_code: BigUint::from_bytes_le(&store.chain_code),
            one_left: store.one_left.as_ref().map(|x| BigUint::from_bytes_le(x)),
            key: BigUint::from_bytes_le(&store.ripemd),
            ripemd_sum: u32::from_be_bytes(store.ripemd_sum),
            history: store.history.into(),
            history_child: store.history.child.into(),
        }
    }
}

impl From<&HierarchyHistory> for KeyHistory {
    fn from(load: &HierarchyHistory) -> Self {
        Self {
            depth: load.child.depth,
            child_number: load.child_number,
            parent_fingerprint: load.child.parent_fingerprint,
        }
    }
}

impl From<&KeyHistory> for HierarchyHistory {
    fn from(store: &KeyHistory) -> Self {
        Self {
            child: HierarchyChild {
                depth: store.depth,
                parent_fingerprint: store.parent_fingerprint,
            },
            child_number: store.child_number,
        }
    }
}

impl From<HierarchyHistory> for KeyHistory {
    fn from(store: HierarchyHistory) -> Self {
        (&store).into()
    }
}

impl From<KeyHistory> for HierarchyHistory {
    fn from(load: KeyHistory) -> Self {
        (&load).into()
    }
}

macro_rules! from_deref_store {
    ($($t:ident)*) => ($(
        concat_idents! (TStore = $t, Store {
            impl From<TStore> for $t {
                fn from(load: TStore) -> Self {
                    (&load).into()
                }
            }
            impl From<$t> for TStore {
                fn from(store: $t) -> Self {
                    (&store).into()
                }
            }
        });
    )*)
}
from_deref_store! { HierarchyChild HierarchyHistory Hierarchy }

#[derive(Serialize, Deserialize, Debug, Clone)]
struct P256R1Store {
    key: BigUint,
    h: BigUint,
    ripemd: BigUint,
    ripemd_sum: u32,
}
impl From<P256R1Store> for P256R1 {
    fn from(load: P256R1Store) -> Self {
        let mut point = [0; 33];
        let bytes = load.h.to_bytes_le();
        point[0..bytes.len()].copy_from_slice(&bytes);
        let mut ret = Self {
            d: load.key,
            h: point.into(),
            ripemd: Default::default(),
            ripemd_sum: load.ripemd_sum.to_be_bytes(),
        };
        let bytes = load.ripemd.to_bytes_le();
        ret.ripemd[0..bytes.len()].copy_from_slice(&bytes);
        ret
    }
}
impl From<P256R1> for P256R1Store {
    fn from(store: P256R1) -> Self {
        Self {
            key: store.d,
            h: BigUint::from_bytes_le(&bp::CompressedPoint::try_from(&store.h).unwrap()),
            ripemd: BigUint::from_bytes_le(&store.ripemd),
            ripemd_sum: u32::from_be_bytes(store.ripemd_sum),
        }
    }
}

#[allow(unused_macros)]
macro_rules! js_serial {
    ($($t:ident)*) => ($(
        impl TryFrom<&JsValue> for $t {
            type Error = serde_json::error::Error;

            fn try_from(load: &JsValue) -> Result<Self, Self::Error> {
                load.into_serde::<Self>()
            }
        }
        impl TryFrom<JsValue> for $t {
            type Error = serde_json::error::Error;

            fn try_from(load: JsValue) -> Result<Self, Self::Error> {
                load.into_serde::<Self>()
            }
        }
        impl TryFrom<&$t> for JsValue {
            type Error = serde_json::error::Error;

            fn try_from(store: &$t) -> Result<Self, Self::Error> {
                Self::from_serde(store)
            }
        }
        impl TryFrom<$t> for JsValue {
            type Error = serde_json::error::Error;

            fn try_from(store: $t) -> Result<Self, Self::Error> {
                Self::from_serde(&store)
            }
        }
    )*)
}

macro_rules! js_serial_store {
    ($($t:ident)*) => ($(
        concat_idents! (TStore = $t, Store {
            impl TryFrom<&JsValue> for TStore {
                type Error = serde_json::error::Error;

                fn try_from(load: &JsValue) -> Result<Self, Self::Error> {
                    load.into_serde::<Self>()
                }
            }
            impl TryFrom<JsValue> for TStore {
                type Error = serde_json::error::Error;

                fn try_from(load: JsValue) -> Result<Self, Self::Error> {
                    load.into_serde::<Self>()
                }
            }
            impl TryFrom<&TStore> for JsValue {
                type Error = serde_json::error::Error;

                fn try_from(store: &TStore) -> Result<Self, Self::Error> {
                    Self::from_serde(store)
                }
            }
            impl TryFrom<TStore> for JsValue {
                type Error = serde_json::error::Error;

                fn try_from(store: TStore) -> Result<Self, Self::Error> {
                    Self::from_serde(&store)
                }
            }
            impl TryFrom<&JsValue> for $t {
                type Error = serde_json::error::Error;

                fn try_from(load: &JsValue) -> Result<Self, Self::Error> {
                    Ok(TStore::try_from(load)?.into())
                }
            }
            impl TryFrom<JsValue> for $t {
                type Error = serde_json::error::Error;

                fn try_from(load: JsValue) -> Result<Self, Self::Error> {
                    Ok(TStore::try_from(load)?.into())
                }
            }
            impl TryFrom<$t> for JsValue {
                type Error = serde_json::error::Error;

                fn try_from(store: $t) -> Result<Self, Self::Error> {
                    use std::convert::TryInto;
                    TStore::from(store).try_into()
                }
            }
        });
    )*)
}

js_serial_store! { P256R1 HierarchyChild HierarchyHistory Hierarchy }

thread_local! {
    static DB: Rc<RefCell<Option<Database>>> = Default::default();
}

async fn get_db() -> Result<Database, JsValue> {
    let x = DB.with(|db| db.clone());
    let mut x = x.borrow_mut();
    Ok(if let Some(x) = &*x {
        x.clone()
    } else {
        let db = idb::open(DB_NAME.to_owned(), NonZeroU32::new(1u32).unwrap()).await?;
        *x = Some(db.clone());
        db
    })
}

fn upgrade_key_db(e: WebEvent) -> Result<(), JsValue> {
    #[allow(unused_unsafe)]
    unsafe {
        web_sys::console::log_1(&"upgrade_key_db".into())
    };
    let db = e
        .target()
        .unwrap()
        .dyn_into::<IdbOpenDbRequest>()?
        .result()?
        .dyn_into::<IdbDatabase>()?;
    let mut params = IdbObjectStoreParameters::new();
    params.key_path(Some(&"key".into()));
    let mut unique = IdbIndexParameters::new();
    unique.unique(true);
    let names = db.object_store_names();
    if !names.contains(OS_P256R1) {
        let os = db.create_object_store_with_optional_parameters(OS_P256R1, &params)?;
        os.create_index_with_str_and_optional_parameters(INDEX_RIPEMD, INDEX_RIPEMD, &unique)?;
        os.create_index_with_str(INDEX_RIPEMD_SUM, INDEX_RIPEMD_SUM)?;
    }
    if !names.contains(OS_HIERARCHY) {
        let os = db.create_object_store_with_optional_parameters(OS_HIERARCHY, &params)?;
        os.create_index_with_str_and_optional_parameters(INDEX_RIPEMD, "key", &unique)?;
        os.create_index_with_str(INDEX_RIPEMD_SUM, INDEX_RIPEMD_SUM)?;
        os.create_index_with_str_and_optional_parameters(
            INDEX_HIERARCHY_HISTORY,
            "history",
            &unique,
        )?;
        os.create_index_with_str(INDEX_HIERARCHY_CHILD, "history_child")?;
    }
    Ok(())
}

// Called by our JS entry point to run the example
pub fn init() {
    idb::UPGRADE.with(|u| {
        u.borrow_mut().insert(
            DB_NAME.to_owned(),
            Box::new(upgrade_key_db) as Box<dyn FnOnce(WebEvent) -> Result<(), JsValue>>,
        );
    });
}
