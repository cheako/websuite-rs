#![feature(async_closure)]

mod utils;

use wasm_bindgen::JsCast;
// use rand::{rngs::OsRng, Rng};
// use js_sys::Promise;
use wasm_bindgen::prelude::{wasm_bindgen, Closure, JsValue};
// use wasm_bindgen::JsCast;
// use web_sys::{
//    CanvasRenderingContext2d, Document, HtmlCanvasElement, HtmlElement, HtmlImageElement,
//    HtmlInputElement, MouseEvent, Touch, TouchEvent, Window,
// };
use web_sys::HtmlElement;

// use std::cell::RefCell;
// use std::rc::Rc;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

macro_rules! derive_handler {
    ($d:expr, $str:expr, $req:expr) => {
        let cb = Closure::wrap(Box::new(move || {
            wasm_bindgen_futures::spawn_local(async move {
                rural::send_message($req).await;
            })
        }) as Box<dyn FnMut()>);
        $d.get_element_by_id($str)
            .expect(&format!("should have #{} on the page", $str))
            .dyn_ref::<HtmlElement>()
            .expect(&format!("#{} be an `HtmlElement`", $str))
            .set_onclick(Some(cb.as_ref().unchecked_ref()));
        cb.forget();
    };
}

/* thread_local! {
 *    static BODY: HtmlElement = DOCUMENT.with(|d|d.body().expect("document should have a body"));
 * } */

// Called by our JS entry point to run the example
#[wasm_bindgen]
pub async fn run() -> Result<(), JsValue> {
    use rural::commute::{AnimalType::*, Request::*};
    utils::set_panic_hook();

    rural::init();
    // rural::configure_header()?;
    rural::key_redirect();
    wasm_bindgen_futures::JsFuture::from(rural::register_service_worker().await?).await?;

    rural::DOCUMENT.with(|d| {
        derive_handler!(
            d,
            "ding",
            Ding(
                // rural::idb_marshal::get_next_private(
                rural::get_key().unwrap(),
                //    &[-rural::get_profile_id(), 1]
                // )
                // .await
                // .unwrap()
            )
        );
        derive_handler!(
            d,
            "cow",
            Animal(
                // rural::idb_marshal::get_next_private(
                rural::get_key().unwrap(),
                //    &[-rural::get_profile_id(), 1]
                // )
                // .await
                // .unwrap()
                Cow
            )
        );
        derive_handler!(
            d,
            "dog",
            Animal(
                // rural::idb_marshal::get_next_private(
                rural::get_key().unwrap(),
                // &[-rural::get_profile_id(), 1]
                // )
                // .await
                // .unwrap()
                Dog
            )
        );
        derive_handler!(
            d,
            "turkey",
            Animal(
                // rural::idb_marshal::get_next_private(
                rural::get_key().unwrap(),
                // &[-rural::get_profile_id(), 1]
                // )
                // .await
                // .unwrap(),
                Turkey
            )
        );
    });

    Ok(())
}
