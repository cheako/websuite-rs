pub mod rural_protobuf;
pub mod srv_protobuf;
pub use jtm;
pub use jtm::{
    brainpool_p256r1::{CompressedPoint, Point as JTMPoint},
    num_bigint::BigUint,
};
pub use protobuf::{self, parse_from_bytes, Message};
pub use rural_protobuf as rural;
pub use srv_protobuf as srv;
use std::convert::TryFrom;
use std::ops::Deref;

#[cfg(feature = "with-serde")]
#[macro_use]
extern crate serde;

pub struct U128(pub [u8; 16]);

impl Deref for U128 {
    type Target = [u8; 16];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl From<&U128> for srv::U128 {
    fn from(data: &U128) -> Self {
        use std::convert::TryInto;
        let mut ret = srv::U128::new();
        ret.set_a(u64::from_be_bytes(data[0..8].try_into().unwrap()));
        ret.set_b(u64::from_be_bytes(data[8..16].try_into().unwrap()));
        ret
    }
}

impl From<&srv::U128> for U128 {
    fn from(data: &srv::U128) -> Self {
        let mut ret = [0; 16];
        ret[0..8].copy_from_slice(&data.get_a().to_be_bytes());
        ret[8..16].copy_from_slice(&data.get_b().to_be_bytes());
        Self(ret)
    }
}

pub struct U160(pub [u8; 20]);

impl Deref for U160 {
    type Target = [u8; 20];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl From<&U160> for srv::U160 {
    fn from(data: &U160) -> Self {
        use std::convert::TryInto;
        let mut ret = srv::U160::new();
        ret.set_a(u64::from_be_bytes(data[0..8].try_into().unwrap()));
        ret.set_b(u64::from_be_bytes(data[8..16].try_into().unwrap()));
        ret.set_c(u32::from_be_bytes(data[16..20].try_into().unwrap()));
        ret
    }
}

impl From<&srv::U160> for U160 {
    fn from(data: &srv::U160) -> Self {
        let mut ret = [0; 20];
        ret[0..8].copy_from_slice(&data.get_a().to_be_bytes());
        ret[8..16].copy_from_slice(&data.get_b().to_be_bytes());
        ret[16..20].copy_from_slice(&data.get_c().to_be_bytes());
        Self(ret)
    }
}

pub struct U256(pub [u8; 32]);

impl Deref for U256 {
    type Target = [u8; 32];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl From<&U256> for srv::U256 {
    fn from(data: &U256) -> Self {
        use std::convert::TryInto;
        let mut ret = srv::U256::new();
        ret.set_a(u64::from_be_bytes(data[0..8].try_into().unwrap()));
        ret.set_b(u64::from_be_bytes(data[8..16].try_into().unwrap()));
        ret.set_c(u64::from_be_bytes(data[16..24].try_into().unwrap()));
        ret.set_d(u64::from_be_bytes(data[24..32].try_into().unwrap()));
        ret
    }
}

impl From<&srv::U256> for U256 {
    fn from(data: &srv::U256) -> Self {
        let mut ret = [0; 32];
        ret[0..8].copy_from_slice(&data.get_a().to_be_bytes());
        ret[8..16].copy_from_slice(&data.get_b().to_be_bytes());
        ret[16..24].copy_from_slice(&data.get_c().to_be_bytes());
        ret[24..32].copy_from_slice(&data.get_d().to_be_bytes());
        Self(ret)
    }
}

impl From<&U256> for BigUint {
    fn from(data: &U256) -> Self {
        BigUint::from_bytes_be(&data.0)
    }
}

impl TryFrom<&BigUint> for U256 {
    type Error = std::array::TryFromSliceError;

    fn try_from(data: &BigUint) -> Result<Self, Self::Error> {
        use std::convert::TryInto;
        Ok(U256(data.to_bytes_be().as_slice().try_into()?))
    }
}

pub struct Point(pub CompressedPoint);

impl From<&Point> for srv::Point {
    fn from(data: &Point) -> Self {
        use std::convert::TryInto;
        let mut ret = srv::Point::new();
        ret.set_a(u64::from_be_bytes(data.0[0..8].try_into().unwrap()));
        ret.set_b(u64::from_be_bytes(data.0[8..16].try_into().unwrap()));
        ret.set_c(u64::from_be_bytes(data.0[16..24].try_into().unwrap()));
        ret.set_d(u64::from_be_bytes(data.0[24..32].try_into().unwrap()));
        ret.set_e(data.0[32] as _);
        ret
    }
}

impl From<&srv::Point> for Point {
    fn from(data: &srv::Point) -> Self {
        let mut ret = [0; 33];
        ret[0..8].copy_from_slice(&data.get_a().to_be_bytes());
        ret[8..16].copy_from_slice(&data.get_b().to_be_bytes());
        ret[16..24].copy_from_slice(&data.get_c().to_be_bytes());
        ret[24..32].copy_from_slice(&data.get_d().to_be_bytes());
        ret[32] = data.get_e() as _;
        Self(ret)
    }
}

impl From<&Point> for JTMPoint {
    fn from(data: &Point) -> Self {
        JTMPoint::from(&data.0)
    }
}

impl From<&JTMPoint> for Point {
    fn from(data: &JTMPoint) -> Self {
        use std::convert::TryInto;
        Point(data.try_into().unwrap())
    }
}

pub struct Sig {
    pub h: JTMPoint,
    pub r: BigUint,
    pub s: BigUint,
}

impl From<&Sig> for srv::Sig {
    fn from(data: &Sig) -> Self {
        let mut ret = srv::Sig::new();
        ret.set_h((&Point::from(&data.h)).into());
        ret.set_r((&U256::try_from(&data.r).unwrap()).into());
        ret.set_s((&U256::try_from(&data.s).unwrap()).into());
        ret
    }
}

impl From<&srv::Sig> for Sig {
    fn from(data: &srv::Sig) -> Self {
        Sig {
            h: (&Point::from(data.get_h())).into(),
            r: (&U256::from(data.get_r())).into(),
            s: (&U256::from(data.get_s())).into(),
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    #[ignore]
    fn debug() {}

    #[test]
    fn u256() -> Result<(), ()> {
        const TEST: [u8; 32] = [
            0x2B, 0x4D, 0x62, 0x51, 0x65, 0x53, 0x68, 0x56, 0x6D, 0x59, 0x71, 0x33, 0x74, 0x36,
            0x77, 0x39, 0x7A, 0x24, 0x43, 0x26, 0x46, 0x29, 0x4A, 0x40, 0x4E, 0x63, 0x52, 0x66,
            0x55, 0x6A, 0x57, 0x6E,
        ];
        match super::U256::from(&super::srv::U256::from(&super::U256(TEST))) {
            super::U256(TEST) => Ok(()),
            _ => Err(()),
        }
    }
}
