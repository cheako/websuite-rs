fn main() {
    use protobuf_codegen_pure::{Codegen, Customize};
    println!("cargo:rerun-if-changed=rural-protobuf.proto");
    println!("cargo:rerun-if-changed=srv-protobuf.proto");
    Codegen::new()
        .out_dir("src")
        .inputs(&["./rural-protobuf.proto", "./srv-protobuf.proto"])
        .include(".")
        .customize(Customize {
            serde_derive: Some(true),
            ..Default::default()
        })
        .run()
        .expect("protoc");
}
