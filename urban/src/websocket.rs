use std::cell::{RefCell, RefMut};
use std::collections::HashMap;
use std::rc::Rc;

use shuttle::{BinRequest as Request, BinResponse as Response};

#[allow(unused_imports)]
use wasm_bindgen::{
    prelude::{wasm_bindgen, Closure, JsValue},
    JsCast,
};
use web_sys::{
    CloseEvent, Event, MessageEvent, ServiceWorkerGlobalScope, WebSocket as SysWebSocket,
};

const AUTO_RECONNECT_MAX_RETRIES: u32 = 600;
const AUTO_RECONNECT_INTERVAL: i32 = 1000 * 5;
const REQUEST_TIMEOUT: Duration = Duration::from_secs(30);

use std::ops::{Add, AddAssign, Sub, SubAssign};

pub use std::time::*;

#[cfg(not(target_arch = "wasm32"))]
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Instant(std::time::Instant);
#[cfg(not(target_arch = "wasm32"))]
impl Instant {
    pub fn now() -> Self {
        Self(std::time::Instant::now())
    }
    pub fn duration_since(&self, earlier: Instant) -> Duration {
        self.0.duration_since(earlier.0)
    }
    #[allow(dead_code)]
    pub fn elapsed(&self) -> Duration {
        self.0.elapsed()
    }
    pub fn checked_add(&self, duration: Duration) -> Option<Self> {
        self.0.checked_add(duration).map(Self)
    }
    pub fn checked_sub(&self, duration: Duration) -> Option<Self> {
        self.0.checked_sub(duration).map(Self)
    }
}

#[cfg(target_arch = "wasm32")]
#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = Date, js_name = now)]
    fn date_now() -> f64;
}

#[cfg(target_arch = "wasm32")]
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Instant(u64);
#[cfg(target_arch = "wasm32")]
impl Instant {
    pub fn now() -> Self {
        Self(date_now() as u64)
    }
    pub fn duration_since(&self, earlier: Instant) -> Duration {
        Duration::from_millis(self.0 - earlier.0)
    }
    #[allow(dead_code)]
    pub fn elapsed(&self) -> Duration {
        Self::now().duration_since(*self)
    }
    pub fn checked_add(&self, duration: Duration) -> Option<Self> {
        use std::convert::TryInto;
        match duration.as_millis().try_into() {
            Ok(duration) => self.0.checked_add(duration).map(|i| Self(i)),
            Err(_) => None,
        }
    }
    pub fn checked_sub(&self, duration: Duration) -> Option<Self> {
        use std::convert::TryInto;
        match duration.as_millis().try_into() {
            Ok(duration) => self.0.checked_sub(duration).map(|i| Self(i)),
            Err(_) => None,
        }
    }
}

impl Add<Duration> for Instant {
    type Output = Instant;
    fn add(self, other: Duration) -> Instant {
        self.checked_add(other).unwrap()
    }
}
impl Sub<Duration> for Instant {
    type Output = Instant;
    fn sub(self, other: Duration) -> Instant {
        self.checked_sub(other).unwrap()
    }
}
impl Sub<Instant> for Instant {
    type Output = Duration;
    fn sub(self, other: Instant) -> Duration {
        self.duration_since(other)
    }
}
impl AddAssign<Duration> for Instant {
    fn add_assign(&mut self, other: Duration) {
        *self = *self + other;
    }
}
impl SubAssign<Duration> for Instant {
    fn sub_assign(&mut self, other: Duration) {
        *self = *self - other;
    }
}

pub trait WebSocketTrait: Clone {
    fn open(&mut self, _w: &mut RefMut<RcWebSocket<Self>>) {}
    fn message(&mut self, _response: Response) {}
    fn close(&mut self) {}
    fn reopen(&mut self, _w: &mut RefMut<RcWebSocket<Self>>) {}
}

pub struct RcWebSocket<CallBack: 'static + Clone + WebSocketTrait> {
    pub dest: String,
    pub websocket: SysWebSocket,
    pub callback: CallBack,
    pub request_ids: HashMap<u32, Box<dyn FnOnce()>>,
    pub retry_queue: Vec<(Request, Instant)>,
    pub acks: Vec<u32>,
    pub seen: Vec<u32>,
    pub retry_count: u32,
    pub sequence: u32,
    pub initial_connection_established: bool,
    pub connected: bool,
}

pub struct WebSocket<CallBack: 'static + Clone + WebSocketTrait> {
    pub inner: Rc<RefCell<RcWebSocket<CallBack>>>,
}

impl<CallBack: 'static + Clone + WebSocketTrait> WebSocket<CallBack> {
    pub fn new(
        global: &ServiceWorkerGlobalScope,
        dest: String,
        callback: CallBack,
    ) -> Result<Self, JsValue> {
        #[allow(unused_unsafe)]
        unsafe {
            web_sys::console::log_1(&JsValue::from("WebSocket::new()"));
        }
        let websocket = SysWebSocket::new(&dest)?;
        let inner = Rc::new(RefCell::new(RcWebSocket {
            dest,
            websocket,
            callback,
            request_ids: HashMap::new(),
            retry_queue: Vec::new(),
            acks: Vec::new(),
            seen: Vec::new(),
            retry_count: 0,
            sequence: 1, /* Not that we can't use 0. */
            initial_connection_established: false,
            connected: false,
        }));

        let weak_ws = Rc::downgrade(&inner);
        let auto_reconnect_and_retry = Closure::wrap(Box::new(move || {
            if let Some(ws) = weak_ws.upgrade() {
                let mut ws_mut = ws.borrow_mut();
                if ws_mut.connected {
                    use odds::vec::VecExt;
                    let now = Instant::now();
                    let request_ids: Vec<_> = ws_mut.request_ids.keys().copied().collect();
                    let websocket = ws_mut.websocket.clone();
                    let acks = ws_mut.acks.split_off(0);
                    ws_mut.retry_queue.retain_mut(|r| {
                        if !request_ids.contains(&r.0.get_id()) {
                            return false;
                        }

                        if now.duration_since(r.1) > REQUEST_TIMEOUT {
                            use shuttle::Message;
                            websocket
                                .send_with_u8_array(&r.0.write_to_bytes().unwrap())
                                .unwrap();
                            r.1 = now;
                        }
                        true
                    });
                    ws_mut.acks = acks;
                }

                drop(ws_mut);
                if ws.borrow().retry_count < AUTO_RECONNECT_MAX_RETRIES {
                    reconnect_watcher(ws);
                } else {
                    panic!("WebSocket can't connect");
                }
            }
        }) as Box<dyn FnMut()>);
        global.set_interval_with_callback_and_timeout_and_arguments_0(
            auto_reconnect_and_retry.as_ref().unchecked_ref(),
            AUTO_RECONNECT_INTERVAL,
        )?;
        auto_reconnect_and_retry.forget();

        register_listeners(&inner);

        Ok(Self { inner })
    }

    pub fn send(&self, request: Request, callback: Box<dyn FnOnce()>) -> Result<(), JsValue> {
        self.inner.borrow_mut().send(request, callback)
    }
}

impl<CallBack: 'static + Clone + WebSocketTrait> RcWebSocket<CallBack> {
    pub fn send(
        &mut self,
        mut request: Request,
        callback: Box<dyn FnOnce()>,
    ) -> Result<(), JsValue> {
        use shuttle::Message;
        let sequence = self.sequence;
        request.set_id(sequence);

        let bytes = request
            .write_to_bytes()
            .map_err(|e| JsValue::from(format!("{:#?}", e)))?;

        self.request_ids.insert(sequence, callback);
        self.retry_queue.push((request, Instant::now()));

        self.sequence = if sequence < u32::MAX { sequence + 1 } else { 1 };

        if self.connected {
            self.websocket.send_with_u8_array(&bytes)
        } else {
            Ok(())
        }
    }
}

fn register_listeners<CallBack: 'static + Clone + WebSocketTrait>(
    ws: &Rc<RefCell<RcWebSocket<CallBack>>>,
) {
    let ws_ro = ws.borrow();
    let sws = &ws_ro.websocket;
    sws.set_binary_type(web_sys::BinaryType::Arraybuffer);

    let weak_ws = Rc::downgrade(&ws);
    let open = Closure::wrap(Box::new(move |_: Event| {
        #[allow(unused_unsafe)]
        unsafe {
            web_sys::console::log_1(&"open".into());
        }
        if let Some(ws) = weak_ws.upgrade() {
            let mut ws_mut = ws.borrow_mut();
            ws_mut.connected = true;
            ws_mut.retry_count = 0;

            use odds::vec::VecExt;
            let now = Instant::now();
            let request_ids: Vec<_> = ws_mut.request_ids.keys().copied().collect();
            let websocket = ws_mut.websocket.clone();
            ws_mut.retry_queue.retain_mut(|r| {
                if !request_ids.contains(&r.0.get_id()) {
                    return false;
                }

                use shuttle::Message;
                websocket
                    .send_with_u8_array(&r.0.write_to_bytes().unwrap())
                    .unwrap();
                r.1 = now;

                true
            });

            if ws_mut.initial_connection_established {
                ws_mut.callback.clone().reopen(&mut ws_mut);
            } else {
                ws_mut.callback.clone().open(&mut ws_mut);
                ws_mut.initial_connection_established = true;
            }
        }
    }) as Box<dyn FnMut(Event)>);
    sws.set_onopen(Some(open.as_ref().unchecked_ref()));
    open.forget();

    let weak_ws = Rc::downgrade(&ws);
    let message = Closure::wrap(Box::new(move |e: MessageEvent| {
        if let (Some(ws), Ok(response)) = (
            weak_ws.upgrade(),
            shuttle::protobuf::parse_from_bytes::<Response>(
                &js_sys::Uint8Array::new(&e.data()).to_vec(),
            ),
        ) {
            let mut ws_mut = ws.borrow_mut();
            let req_id = &mut ws_mut.request_ids;
            response.get_acks().iter().for_each(|id| {
                if let Some(f) = req_id.remove(id) {
                    f();
                };
            });
            ws_mut.callback.message(response);
        };
    }) as Box<dyn FnMut(MessageEvent)>);
    sws.set_onmessage(Some(message.as_ref().unchecked_ref()));
    message.forget();

    let weak_ws = Rc::downgrade(&ws);
    let close = Closure::wrap(Box::new(move |_: CloseEvent| {
        #[allow(unused_unsafe)]
        unsafe {
            web_sys::console::log_1(&"close".into());
        }
        if let Some(ws) = weak_ws.upgrade() {
            let ws_ro = ws.borrow();
            if ws_ro.connected {
                drop(ws_ro);
                let mut ws_mut = ws.borrow_mut();
                ws_mut.connected = false;
                ws_mut.callback.close();
                drop(ws_mut);
                reconnect_watcher(ws);
            }
        }
    }) as Box<dyn FnMut(CloseEvent)>);
    sws.set_onclose(Some(close.as_ref().unchecked_ref()));
    close.forget();
}

fn reconnect_watcher<CallBack: 'static + Clone + WebSocketTrait>(
    inner: Rc<RefCell<RcWebSocket<CallBack>>>,
) {
    let inner_ro = inner.borrow();
    if !inner_ro.connected && inner_ro.websocket.ready_state() == 3 {
        drop(inner_ro);
        let mut inner_mut = inner.borrow_mut();
        inner_mut.retry_count += 1;
        if let Ok(ws) = SysWebSocket::new(&inner_mut.dest) {
            inner_mut.websocket = ws;
            drop(inner_mut);
            register_listeners(&inner);
        }
    }
}
