#![feature(async_closure)]
#![feature(duration_constants)]
#![feature(refcell_take)]
#![cfg(target_arch = "wasm32")]

use std::cell::{RefCell, RefMut};

mod utils;
mod websocket;

use js_sys::{Array, Promise};
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use wasm_bindgen_futures::JsFuture;
use web_sys::{
    /* console::log_1, */ Cache, Client, Clients, ExtendableEvent, FetchEvent, MessageEvent,
    ServiceWorkerGlobalScope,
};

use shuttle::commute::ListenResponse;
use shuttle::protobuf::srv::Response as BinResponse;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

use websocket::{RcWebSocket, WebSocket};
thread_local! {
    static GLOBAL: ServiceWorkerGlobalScope = js_sys::global().dyn_into()
        .expect("global should be service worker");
    static CLIENTS: Clients = GLOBAL.with(|g| g.clients());
    static WEBSOCKET: WebSocket<Callback> = GLOBAL.with(|g| WebSocket::new(&g,
                "wss://notes.mikemestnik.net/wss".to_owned(), Callback))
        .expect("able to create websocket");
    static LISTENS: RefCell<Vec<shuttle::commute::ListenRequest>> = Default::default();
}

// Called by our JS entry point to run the example
#[wasm_bindgen(start)]
pub fn run() {
    utils::set_panic_hook();
}

#[wasm_bindgen]
pub fn install(event: ExtendableEvent) -> Result<(), JsValue> {
    let promise = wasm_bindgen_futures::future_to_promise(async {
        use std::iter::FromIterator;

        let promise = GLOBAL.with(|g| g.caches()).unwrap().open("offline-v001");
        let cache = Cache::from(JsFuture::from(promise).await?);

        JsFuture::from(
            cache.add_all_with_str_sequence(&Array::from_iter(
                [
                    "urban.js",
                    "urban_bg.wasm",
                    "profile.html",
                    "settings.html",
                    "js/bootstrap-3.3.7.js",
                    "js/fio-util.js",
                    "js/eos-util.js",
                    "js/sjcl-bip39.js",
                    "js/wordlist_chinese_traditional.js",
                    "js/wordlist_italian.js",
                    "js/casinocoin-util.js",
                    "js/ripple-util.js",
                    "js/wordlist_english.js",
                    "js/wordlist_czech.js",
                    "js/index.js",
                    "js/jsbip39.js",
                    "js/wordlist_spanish.js",
                    "js/wordlist_japanese.js",
                    "js/jingtum-util.js",
                    "js/wordlist_french.js",
                    "js/wordlist_korean.js",
                    "js/wordlist_chinese_simplified.js",
                    "js/jquery-3.2.1.js",
                    "js/bip39-libs.js",
                    "js/base64.js",
                    "index.html",
                    "manifest.json",
                    "start.js",
                    "start_bg.wasm",
                    "icons/icon-152x152.png",
                    "icons/icon-128x128.png",
                    "icons/icon-512x512.png",
                    "icons/icon-72x72.png",
                    "icons/icon-96x96.png",
                    "icons/icon-192x192.png",
                    "icons/icon-384x384.png",
                    "icons/icon-144x144.png",
                ]
                .iter()
                .copied()
                .map(JsValue::from),
            )),
        )
        .await?;

        JsFuture::from(GLOBAL.with(|g| g.skip_waiting().unwrap())).await?;
        Ok(JsValue::UNDEFINED)
    });

    event.wait_until(&promise)
}

#[wasm_bindgen]
pub fn activate(event: ExtendableEvent) -> Result<(), JsValue> {
    let claim = Closure::once(
        Box::new(move || CLIENTS.with(|c| c.claim())) as Box<dyn FnOnce() -> Promise>
    );
    event.wait_until(claim.as_ref().unchecked_ref())?;
    claim.forget();

    Ok(())
}

#[wasm_bindgen]
pub fn fetch(event: FetchEvent) -> Result<(), JsValue> {
    let request = event.request();
    if request.method() != "GET" {
        return Ok(());
    }

    let local_event = event.clone();
    // Prevent the default, and handle the request ourselves.
    event.respond_with(&wasm_bindgen_futures::future_to_promise(async move {
        // Try to get the response from a cache.
        let promise = GLOBAL.with(|g| g.caches().unwrap().open("offline-v001"));
        let cache = Cache::from(JsFuture::from(promise).await?);
        let cached_response = JsFuture::from(cache.match_with_request(&request)).await?;

        if cached_response.is_truthy() {
            // If we found a match in the cache, return it, but also
            // update the entry in the cache in the background.
            local_event.wait_until(&cache.add_with_request(&request))?;
            Ok(cached_response)
        } else {
            use wasm_bindgen::JsCast;

            Ok(wasm_bindgen_futures::future_to_promise(async move {
                // If we didn't find a match in the cache, use the network.
                let fetched_response: web_sys::Response =
                    JsFuture::from(GLOBAL.with(|g| g.fetch_with_request(&request)))
                        .await?
                        .unchecked_into();
                JsFuture::from(cache.put_with_request(&request, &fetched_response.clone()?))
                    .await?;

                Ok(fetched_response.into())
            })
            .into())
        }
    }))
}

#[wasm_bindgen]
pub async fn message(event: MessageEvent) -> Result<(), JsValue> {
    use shuttle::{
        build_request,
        JSONRequest::{self, *},
    };
    use std::convert::TryFrom;

    let req = JSONRequest::try_from(&event.data()).unwrap();
    match req {
        Start => {}
        Listen(x) => {
            let mut z = x.clone();
            LISTENS.with(|y| y.borrow_mut().append(&mut z));
            WEBSOCKET.with(|w| {
                let _ = w.send(build_request(&mut Listen(x)), Box::new(|| {}));
            });
        }
        mut x => WEBSOCKET.with(|w| {
            let _ = w.send(build_request(&mut x), Box::new(|| {}));
        }),
    }

    Ok(())
}

#[derive(Clone, Debug)]
pub struct Callback;

impl websocket::WebSocketTrait for Callback {
    fn message(&mut self, response: BinResponse) {
        #[allow(unused_unsafe)]
        unsafe {
            web_sys::console::log_2(
                &"WebSocketTrait::message:".into(),
                &format!("{:#?}", response).into(),
            );
        }
        response.get_updates().iter().for_each(|x| {
            use std::convert::TryInto;
            let d = x.get_insert();
            let inner = d.get_inner();
            /* let seq = inner.get_seq();
            let inner = inner.get_inner();
            let score: u64 = d
            .get_sig()
            .iter()
            .cloned()
            .map(|x| {
                let t = x.get_time();
                if shuttle::test_sign(&x, &mut inner) {
                    t - 1577836800000 /* 2020 */
                } else {
                    0
                }
            })
            .sum(); */
            if let Ok(y) = (&ListenResponse::from((inner, x.get_ctime()))).try_into() {
                wasm_bindgen_futures::spawn_local(async move {
                    if let Ok(x) = JsFuture::from(CLIENTS.with(|c| {
                        c.match_all_with_options(
                            web_sys::ClientQueryOptions::new().type_(web_sys::ClientType::Window),
                        )
                    }))
                    .await
                    {
                        if let Ok(x) = x.dyn_into::<Array>() {
                            x.for_each(&mut |x, _, _| {
                                if let Ok(x) = x.dyn_into::<Client>() {
                                    let _ = x.post_message(&y);
                                };
                            });
                        };
                    };
                });
            };
        });
    }
    fn open(&mut self, w: &mut RefMut<RcWebSocket<Self>>) {
        self.reopen(w);
    }
    fn reopen(&mut self, w: &mut RefMut<RcWebSocket<Self>>) {
        if let Some(v) = LISTENS.with(|y| {
            let y = y.borrow();
            if y.is_empty() {
                None
            } else {
                Some(y.clone())
            }
        }) {
            let _ = w.send(
                shuttle::build_request(&mut shuttle::JSONRequest::Listen(v)),
                Box::new(|| {}),
            );
        }
    }
}
