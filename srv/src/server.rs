use std::ops::Deref;
use std::sync::{
    atomic::{AtomicUsize, Ordering},
    Mutex,
};

#[allow(unused_imports)]
use futures::{FutureExt, StreamExt};
#[allow(unused_imports)]
use shuttle::protobuf::{
    self,
    jtm::brainpool_p256r1 as bp,
    protobuf::RepeatedField,
    srv::{ListenResponse, Request, Response},
    U160, U256,
};
use tokio::sync::{broadcast, mpsc};
use warp::ws::{Message, WebSocket};

#[derive(Debug, Clone)]
pub struct Channel(broadcast::Sender<Message>);

impl Deref for Channel {
    type Target = broadcast::Sender<Message>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Default for Channel {
    fn default() -> Self {
        Self(broadcast::channel(256).0)
    }
}

impl naomi::normal::Value for Channel {
    fn is_empty(&mut self) -> bool {
        self.receiver_count() == 0
    }
}

pub type Channels = naomi::normal::Factory<u64, Channel>;

/// Our global unique user id counter.
static NEXT_USER_ID: AtomicUsize = AtomicUsize::new(1);

struct User {
    id: usize,
    listen: Mutex<Vec<mpsc::Sender<()>>>,
    #[allow(dead_code)]
    tx: mpsc::UnboundedSender<Result<Message, warp::Error>>,
}

impl Drop for User {
    fn drop(&mut self) {
        self.listen.lock().unwrap().iter_mut().for_each(|x| {
            let _ = x.send(());
        });
    }
}

pub async fn user_connected(ws: WebSocket, channels: Channels) {
    // Split the socket into a sender and receive of messages.
    let (user_ws_tx, mut user_ws_rx) = ws.split();

    // Use an unbounded channel to handle buffering and flushing of messages
    // to the websocket...
    let (tx, rx) = mpsc::unbounded_channel::<Result<Message, warp::Error>>();
    tokio::task::spawn(rx.forward(user_ws_tx).map(|result| {
        if let Err(e) = result {
            eprintln!("websocket send error: {}", e);
        }
    }));

    let user = User {
        // Use a counter to assign a new unique ID for this user.
        id: NEXT_USER_ID.fetch_add(1, Ordering::Relaxed),
        listen: Default::default(),
        tx,
    };

    // Return a `Future` that is basically a state machine managing
    // this specific user's connection.

    // Every time the user sends a message, broadcast it to
    // all other users...
    while let Some(result) = user_ws_rx.next().await {
        let msg = match result {
            Ok(msg) => msg,
            Err(e) => {
                eprintln!("websocket error: {}", e);
                break;
            }
        };
        user_message(&user, msg, &channels).await;
    }

    // user_ws_rx stream will keep processing as long as the user stays
    // connected. Once they disconnect, then...
    user_disconnected(user).await;
}

async fn user_message(user: &User, msg: Message, channels: &Channels) {
    if msg.is_close() || msg.is_ping() || msg.is_pong() {
        return;
    }

    if let Ok(request) = shuttle::protobuf::protobuf::parse_from_bytes::<Request>(msg.as_bytes()) {
        // TODO: Write 2 disk.

        eprintln!("{:#?}", request);
        user.listen.lock().unwrap().append(
            &mut request
                .get_listen()
                .iter()
                .map(|l| {
                    let receiver = channels.map(l.get_listen_on_key(), &mut |v| {
                        std::sync::Arc::new(tokio::sync::Mutex::new(broadcast::Sender::subscribe(
                            v,
                        )))
                    });
                    let (close_tx, mut close_rx) = mpsc::channel::<()>(1);
                    let ws_tx = user.tx.clone();
                    tokio::spawn(async move {
                        let r = receiver.clone();
                        if let Ok(metadata) = std::fs::metadata("foo.txt") {
                            if let Ok(time) = metadata.created() {
                                println!("{:?}", time);
                            } else {
                                println!("Not supported on this platform or filesystem");
                            }
                        }
                        loop {
                            let mut r = r.lock().await;
                            tokio::select! {
                                _ = close_rx.recv() => { break }
                                x = r.recv() => match x {
                                    Ok(m) => if let Err(e) = ws_tx.send(Ok(m)) {
                                        eprintln!("mpsc to websocket send error: {}", e);
                                    }
                                    Err(e) => { eprintln!("broadcast receive error: {}", e); }
                                }
                            };
                        }
                        drop(receiver);
                    });

                    close_tx
                })
                .collect(),
        );
        request.get_messages().iter().for_each(|message| {
            use shuttle::Message as Thing;
            use std::convert::{TryFrom, TryInto};
            use std::hash::Hasher;
            let inner = message.get_inner();
            let sig = message.get_sig();
            if !shuttle::test_sign(&sig, inner) {
                eprintln!("test_sign: opps");
                return;
            }
            let ripemd = bp::RIPEMD160::try_from(bp::Point::from(
                protobuf::Point::from(message.get_sig().get_h()).0,
            ))
            .unwrap();
            let mut path = std::path::PathBuf::from("/home/srv_websuite/var/share");
            path.push(&base64::encode_config(&ripemd[0..8], base64::URL_SAFE));
            std::fs::create_dir_all(&path).unwrap();
            #[allow(deprecated)]
            let mut siphash = std::hash::SipHasher::new();
            siphash.write(&*U256::from(sig.get_s()));
            path.push(&base64::encode_config(
                &[&ripemd[8..], &siphash.finish().to_be_bytes()].concat(),
                base64::URL_SAFE,
            ));
            let mut file = std::fs::File::create(&path).unwrap();
            let mut cos = protobuf::protobuf::CodedOutputStream::new(&mut file);

            message.write_to(&mut cos).unwrap();
            cos.flush().unwrap();

            eprintln!("{:#?}", channels);
            channels.map(
                if false {
                    u64::from_be_bytes(ripemd[0..8].try_into().unwrap())
                } else {
                    4u64
                },
                &mut |v| {
                    let mut r = Response::new();
                    let mut inner = ListenResponse::new();
                    inner.set_message(message.clone());
                    r.set_updates(RepeatedField::from_vec(vec![inner]));
                    let _ = v.send(Message::binary(r.write_to_bytes().unwrap()));
                },
            );
        });
        request.get_sig_messages().iter().for_each(|message| {use std::hash::Hasher;
            use shuttle::Message as Thing;
            use std::convert::{TryFrom, TryInto};
            let inner = message.get_inner();
            let sig = message.get_sig();
            inner.get_key();
            inner.get_key_sum();
            if !shuttle::sig_test_sign(&sig, inner) {
                eprintln!("sig_test_sign: opps");
                return;
            }
            let ripemd = bp::RIPEMD160::try_from(bp::Point::from(
                protobuf::Point::from(message.get_sig().get_h()).0,
            ))
            .unwrap();
            let mut path = std::path::PathBuf::from("/home/srv_websuite/var/share");
            path.push(&base64::encode_config(&ripemd[0..8], base64::URL_SAFE));
            std::fs::create_dir_all(&path).unwrap();
            #[allow(deprecated)]
            let mut siphash = std::hash::SipHasher::new();
            siphash.write(&*U256::from(sig.get_s()));
            path.push(&base64::encode_config(
                &[&ripemd[8..], &siphash.finish().to_be_bytes()].concat(),
                base64::URL_SAFE,
            ));
            let mut file = std::fs::File::create(&path).unwrap();
            let mut cos = protobuf::protobuf::CodedOutputStream::new(&mut file);

            message.write_to(&mut cos).unwrap();
            cos.flush().unwrap();

            eprintln!("{:#?}", channels);
            channels.map(
                if false {
                    u64::from_be_bytes(ripemd[0..8].try_into().unwrap())
                } else {
                    4u64
                },
                &mut |v| {
                    let mut r = Response::new();
                    let mut inner = ListenResponse::new();
                    inner.set_sig_message(message.clone());
                    r.set_updates(RepeatedField::from_vec(vec![inner]));
                    let _ = v.send(Message::binary(r.write_to_bytes().unwrap()));
                },
            );
        });
        let mut r = Response::new();
        r.set_acks(vec![request.get_id()]);
        let _ = user.tx.send(Ok(Message::binary(
            {
                use shuttle::Message;
                r.write_to_bytes()
            }
            .unwrap(),
        )));
    }
}

async fn user_disconnected(user: User) {
    eprintln!("good bye user: {}", user.id);
}
