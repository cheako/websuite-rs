use yew::prelude::*;
use yew_router::prelude::*;

use crate::route::{Route, RouteInner};

pub struct TopNav {
    page: RouteInner,
}

#[derive(Properties, Clone, PartialEq)]
pub struct TopNavProperties {
    #[prop_or(RouteInner::HomePage)]
    pub page: RouteInner,
}

impl Component for TopNav {
    type Message = ();
    type Properties = TopNavProperties;

    fn create(props: Self::Properties, _link: ComponentLink<Self>) -> Self {
        Self { page: props.page }
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        // let onclick = self.props.on_add_to_cart.reform(|_| ());

        html! {
        <div class="topnav">
            <RouterAnchor<Route> classes={ if self.page == RouteInner::HomePage { "active" } else { "" } }
                route=Route(RouteInner::HomePage)>{"Home"}</RouterAnchor<Route>>
            <RouterAnchor<Route> classes={ if self.page == RouteInner::Profile { "active" } else { "" } }
                route=Route(RouteInner::Profile)>{"Profile"}</RouterAnchor<Route>>
            <RouterAnchor<Route> classes={ if self.page == RouteInner::Settings { "active" } else { "" } }
                route=Route(RouteInner::Settings)>{"Settings"}</RouterAnchor<Route>>
            <RouterAnchor<Route> classes={ if self.page == RouteInner::Debug { "active" } else { "" } }
                route=Route(RouteInner::Debug)>{"Debug"}</RouterAnchor<Route>>
            <div class="right"><select id="profile_id">
                    <option value="new">{ "New..." }</option>
                </select></div>
        </div>
            }
    }
}
