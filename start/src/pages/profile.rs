use rural::PrivKey;
use wasm_bindgen::{
    prelude::{Closure, JsValue},
    JsCast,
};
use web_sys::{Document, HtmlButtonElement, Window};
use yew::prelude::*;

use crate::components::TopNav;
use crate::route::RouteInner as Route;

thread_local! {
    static WINDOW: Window = web_sys::window().expect("should have a window in this context");
    static DOCUMENT: Document = WINDOW.with(|w|w.document().expect("window should have a document"));
}

pub struct Profile {}

impl Component for Profile {
    type Message = ();
    type Properties = ();

    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self {}
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        html! { <><TopNav page=Route::Profile />
            <button type="button" id="ding">{ "Ding" }</button><br/>
            <button type="button" id="cow">{ "Cow" }</button><br/>
            <button type="button" id="dog">{ "Dog" }</button><br/>
            <button type="button" id="turkey">{ "Turkey" }</button><br/>
        </> }
    }

    fn rendered(&mut self, _first_render: bool) {
        use rural::{
            commute::{AnimalType, Request::*},
            send_message as send,
        };
        let buttons: [HtmlButtonElement; 4] = DOCUMENT.with(|d| {
            [
                d.get_element_by_id("ding")
                    .expect("Document should have id 'ding'")
                    .dyn_into()
                    .expect("ding to be HtmlButtonElement"),
                d.get_element_by_id("cow")
                    .expect("Document should have id 'cow'")
                    .dyn_into()
                    .expect("cow to be HtmlButtonElement"),
                d.get_element_by_id("dog")
                    .expect("Document should have id 'dog'")
                    .dyn_into()
                    .expect("dog to be HtmlButtonElement"),
                d.get_element_by_id("turkey")
                    .expect("Document should have id 'turkey'")
                    .dyn_into()
                    .expect("turkey to be HtmlButtonElement"),
            ]
        });

        let ding_callback = Closure::wrap(Box::new(move |_| {
            wasm_bindgen_futures::spawn_local(async move {
                let _ = send(Ding(key().await, Vec::default())).await;
            });
        }) as Box<dyn FnMut(JsValue)>);
        let cow_callback = Closure::wrap(Box::new(move |_| {
            wasm_bindgen_futures::spawn_local(async move {
                let _ = send(Animal(key().await, AnimalType::Cow, Vec::default())).await;
            });
        }) as Box<dyn FnMut(JsValue)>);
        let dog_callback = Closure::wrap(Box::new(move |_| {
            wasm_bindgen_futures::spawn_local(async move {
                let _ = send(Animal(key().await, AnimalType::Dog, Vec::default())).await;
            });
        }) as Box<dyn FnMut(JsValue)>);
        let turkey_callback = Closure::wrap(Box::new(move |_| {
            wasm_bindgen_futures::spawn_local(async move {
                let _ = send(Animal(key().await, AnimalType::Turkey, Vec::default())).await;
            });
        }) as Box<dyn FnMut(JsValue)>);
        buttons[0].set_onclick(Some(ding_callback.as_ref().unchecked_ref()));
        buttons[1].set_onclick(Some(cow_callback.as_ref().unchecked_ref()));
        buttons[2].set_onclick(Some(dog_callback.as_ref().unchecked_ref()));
        buttons[3].set_onclick(Some(turkey_callback.as_ref().unchecked_ref()));
        // forget the callbacks to keep them alive
        ding_callback.forget();
        cow_callback.forget();
        dog_callback.forget();
        turkey_callback.forget();
    }
}

async fn key() -> PrivKey {
    let mut pkey = rural::get_key().unwrap();
    let mut pkey = rural::commute::idb::ckd_priv(&mut pkey, -1).await;
    rural::commute::idb::ckd_priv(&mut pkey, 1).await
}
