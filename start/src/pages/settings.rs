use wasm_bindgen::{
    prelude::{wasm_bindgen, Closure},
    JsCast,
};
use web_sys::{HtmlButtonElement, HtmlInputElement, HtmlTextAreaElement};
use yew::prelude::*;

use num_bigint_dig::BigUint;

#[wasm_bindgen(raw_module = "./js/index.js")]
extern "C" {
    fn dom_rendered();
}

use crate::components::TopNav;
use crate::route::RouteInner as Route;

use rural::{ChainCode, PrivKey, PrivKeyBytes, PrivKeyStorage, DOCUMENT};

pub struct Settings {}

impl Component for Settings {
    type Message = ();
    type Properties = ();

    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self {}
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        html! { <><TopNav page=Route::Settings />
        <div class="feedback-container">
            <div id="feedback" class="feedback">{ "Loading..." }</div>
        </div>
        <form class="form-horizontal" role="form">
            <div class="form-group">
                <div class="col-sm-2"></div>
                <div class="col-sm-10">
                    <p>{ "You can enter an existing BIP39 mnemonic, or generate a new random one. Typing your own twelve words
                    will probably not work how you expect, since the words require a particular structure (the last word
                    contains a checksum)." }</p>
                </div>
            </div>
            <div class="form-group generate-container">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                    <div class="form-inline">
                        <div class="input-group-inline">
                            <span>{ "Generate a random mnemonic" }</span>{ ":" }
                            <button id="generate" class="btn generate"><b>{ "GENERATE" }</b></button>
                            <select id="strength" class="strength form-control">
                                <option value=3>{ 3 }</option>
                                <option value=6>{ 6 }</option>
                                <option value=9>{ 9 }</option>
                                <option value=12>{ 12 }</option>
                                <option value=15 selected=true>{ 15 }</option>
                                <option value=18>{ 18 }</option>
                                <option value=21>{ 21 }</option>
                                <option value=24>{ 24 }</option>
                            </select>
                            <span>{ "words, or enter your own below" }</span>{ "." }
                            <p id="generate-container-warning" class="warning help-block hidden">
                                <span class="text-danger"> { "Mnemonics with less than 12 words have low entropy and may be guessed." }</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2"></div>
                <div class="col-sm-10">
                    <p>{ "You should only press on Apply \"once\" for each user on a given browser instalation. For multiple
                    users sharing the same browser, measures should be taken such that the browser treats you all as
                    individual ppl. The choices here are saved in the IndexedDB and localStorage. If your browser is
                    confusing these accross users this should be reported as a bug, to the browser vendor." }</p>
                    <p>{ "Note: The BIP39 Mnemonic and BIP39 Passphrase(a password) are saved for use ONLY when reloading this
                        page. The resulting BIP32 Root Key is used to Eyncript and Sign data for use by this app." }</p>
                </div>
            </div>
            <div class="form-group apply-container">
                <label class="col-sm-2 control-label">{ "Apply will replace the keys with the ones displayed, without a backup
                    they cannot be restored." }</label>
                <div class="col-sm-10">
                    <button id="apply" class="btn apply"><b>{ "APPLY" }</b></button>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">{ "Mnemonic Language" }</label>
                <div id="languages" class="col-sm-10 languages">
                    <div class="form-control no-border">
                        <a href="#english">{ "English" }</a>
                        <a href="#japanese" title="Japanese">{ "日本語" }</a>
                        <a href="#spanish" title="Spanish">{ "Español" }</a>
                        <a href="#chinese_simplified" title="Chinese (Simplified)">{ "中文(简体)" }</a>
                        <a href="#chinese_traditional" title="Chinese (Traditional)">{ "中文(繁體)" }</a>
                        <a href="#french" title="French">{ "Français" }</a>
                        <a href="#italian" title="Italian">{ "Italiano" }</a>
                        <a href="#korean" title="Korean">{ "한국어" }</a>
                        <a href="#czech" title="Czech">{ "Čeština" }</a>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="phrase" class="col-sm-2 control-label">{ "BIP39 Mnemonic" }</label>
                <div class="col-sm-10">
                    <textarea id="phrase" class="phrase private-data form-control" data-show-qr="" autocomplete="off"
                        autocorrect="off" autocapitalize="off" spellcheck="false"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="phrase" class="col-sm-2 control-label">{ "BIP39 Split Mnemonic" }</label>
                <div class="col-sm-10">
                    <textarea id="phraseSplit" class="phraseSplit private-data form-control"
                        title="Only 2 of 3 cards needed to recover." rows="3" disabled=true></textarea>
                    <p class="help-block">
                        <span id="phraseSplitWarn" class="phraseSplitWarn"></span>
                    </p>
                </div>
            </div>
            <div class="form-group">
                <label for="passphrase" class="col-sm-2 control-label">{ "BIP39 Passphrase (optional, is saved)" }</label>
                <div class="col-sm-10">
                    <textarea id="passphrase" class="passphrase private-data form-control" autocomplete="off"
                        autocorrect="off" autocapitalize="off" spellcheck="false"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="seed" class="col-sm-2 control-label">{ "BIP39 Seed" }</label>
                <div class="col-sm-10">
                    <textarea id="seed" class="seed private-data form-control" data-show-qr="" autocomplete="off"
                        autocorrect="off" autocapitalize="off" spellcheck="false"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="root-key" class="col-sm-2 control-label">{ "BIP32 Root Key" }</label>
                <div class="col-sm-10">
                    <textarea id="root-key" class="root-key private-data form-control" data-show-qr="" autocomplete="off"
                        autocorrect="off" autocapitalize="off" spellcheck="false"></textarea>
                    <input type="hidden" id="d" value=""/><input type="hidden" id="chain-code" value=""/>
                </div>
            </div>
        </form>
        <div id="qr-container" class="qr-container hidden">
            <div class="qr-hint bg-primary hidden">{ "Click field to hide QR" }</div>
            <div class="qr-hint bg-primary">{ "Click field to show QR" }</div>
            <div id="qr-hider" class="qr-hider hidden">
                <div id="qr-image" class="qr-image"></div>
                <div class="qr-warning bg-primary">{ "Caution: Scanner may keep history" }</div>
            </div>
        </div>
            </> }
    }

    fn rendered(&mut self, _first_render: bool) {
        #[allow(unused_unsafe)]
        unsafe {
            dom_rendered();
        }

        let phrase: (HtmlTextAreaElement, HtmlTextAreaElement) = (
            DOCUMENT
                .with(|d| d.get_element_by_id("phrase"))
                .expect("Document should have id 'phrase'")
                .dyn_into()
                .expect("phrase to be HtmlTextAreaElement"),
            DOCUMENT
                .with(|d| d.get_element_by_id("passphrase"))
                .expect("Document should have id 'passphrase'")
                .dyn_into()
                .expect("passphrase to be HtmlTextAreaElement"),
        );

        if let Some(Ok(pkey)) = rural::STORAGE
            .with(|s| s.get_item("key_serde"))
            .expect("get_item key_json")
            .map(|ref json| serde_json::from_str::<PrivKeyStorage>(json))
        {
            phrase.0.set_value(&pkey.phrase);
            phrase.1.set_value(&pkey.passphrase);
            phrase
                .0
                .dispatch_event(&web_sys::CustomEvent::new("change").unwrap())
                .unwrap();
        }

        let apply_callback = Closure::wrap(Box::new(move || {
            let (d, chain_code): (HtmlInputElement, HtmlInputElement) = DOCUMENT.with(|d| {
                (
                    d.get_element_by_id("d")
                        .expect("Document should have id 'd'")
                        .dyn_into()
                        .expect("d to be HtmlInputElement"),
                    d.get_element_by_id("chain-code")
                        .expect("Document should have id 'chain-code'")
                        .dyn_into()
                        .expect("chain-code to be HtmlInputElement"),
                )
            });
            use num_traits::Num;
            rural::STORAGE
                .with(|s| {
                    use std::convert::TryFrom;
                    s.set_item(
                        "key_serde",
                        &serde_json::to_string(&PrivKeyStorage {
                            phrase: phrase.0.value(),
                            passphrase: phrase.1.value(),
                            d: rural::bs58::encode(
                                &PrivKeyBytes::from(&PrivKey::new(
                                    BigUint::from_str_radix(&d.value(), 36)
                                        .expect("JS to supply valid d number"),
                                    ChainCode::try_from(
                                        &base64::decode(&chain_code.value())
                                            .expect("chain-code to contain base64")
                                            [0..32],
                                    )
                                    .unwrap(),
                                    None,
                                    Default::default(),
                                ))[73 - 64..73],
                            )
                            .into_string(),
                        })
                        .expect("serde_json<PrivKey>::to_string()"),
                    )
                })
                .expect("set_item key_json");
        }) as Box<dyn FnMut()>);
        DOCUMENT
            .with(|d| d.get_element_by_id("apply"))
            .expect("Document should have id 'apply'")
            .dyn_into::<HtmlButtonElement>()
            .expect("apply to be HtmlButtonElement")
            .set_onclick(Some(apply_callback.as_ref().unchecked_ref()));
        // forget the callback to keep it alive
        apply_callback.forget();
    }
}
