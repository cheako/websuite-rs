#[allow(unused_imports)]
use rural::{bs58, PrivKey, PubKey};
use wasm_bindgen::{
    prelude::{Closure, JsValue},
    JsCast,
};
use web_sys::{Document, HtmlButtonElement, HtmlDivElement, HtmlInputElement, Window};
use yew::prelude::*;

use crate::components::TopNav;
use crate::route::RouteInner as Route;

thread_local! {
    static WINDOW: Window = web_sys::window().expect("should have a window in this context");
    static DOCUMENT: Document = WINDOW.with(|w|w.document().expect("window should have a document"));
}

pub struct Debug {}

impl Component for Debug {
    type Message = ();
    type Properties = ();

    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self {}
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        html! { <><TopNav page=Route::Debug />
            <input type="number" id="key" min="0" max="100" />
            <button type="button" id="listen">{ "Listen" }</button><br/>
            <div id="loading" />
        </> }
    }

    fn rendered(&mut self, _first_render: bool) {
        use js_sys::Promise;
        #[allow(unused_imports)]
        use rural::{
            commute::{AnimalType, Request::*},
            send_message_promise as send,
        };
        let button: HtmlButtonElement = DOCUMENT.with(|d| {
            d.get_element_by_id("listen")
                .expect("Document should have id 'listen'")
                .dyn_into()
                .expect("listen to be HtmlButtonElement")
        });

        let key: HtmlInputElement = DOCUMENT.with(|d| {
            d.get_element_by_id("key")
                .expect("Document should have id 'key'")
                .dyn_into()
                .expect("key to be HtmlInputElement")
        });

        let listen_callback =
            Closure::wrap(
                Box::new(move |_| send(Listen(vec![(0, key.value_as_number() as u64)])))
                    as Box<dyn FnMut(JsValue) -> Promise>,
            );
        button.set_onclick(Some(listen_callback.as_ref().unchecked_ref()));
        // forget the callbacks to keep them alive
        listen_callback.forget();

        let _loading: HtmlDivElement = DOCUMENT.with(|d| {
            d.get_element_by_id("loading")
                .expect("Document should have id 'loading'")
                .dyn_into()
                .expect("loading to be HtmlDivElement")
        });

        wasm_bindgen_futures::spawn_local(async {
            // Wpriv(77) 0x45f606a2
            // Wpub(78) 0x0fd9db45
            let mut key = get_current_key().await;
            let priv_ = rural::PrivKeyBytes::from(&key);
            let pub_ = rural::PubKeyBytes::from(&PubKey::from(&mut key));
            #[allow(unused_unsafe)]
            unsafe {
                web_sys::console::log_1(
                    &format!(
                        "{}\n{}",
                        bs58::encode(&[&u32::to_be_bytes(0x45f606a2)[..], &priv_].concat())
                            .into_string(),
                        bs58::encode(&[&u32::to_be_bytes(0x0fd9db45)[..], &pub_].concat())
                            .into_string()
                    )
                    .into(),
                );
            }
        });
    }
}

#[allow(dead_code)]
async fn get_current_key() -> PrivKey {
    let mut privkey = rural::get_key().unwrap();
    let mut privkey = rural::commute::idb::ckd_priv(&mut privkey, -1).await;
    rural::commute::idb::ckd_priv(&mut privkey, -1).await
}
