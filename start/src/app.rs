use yew::prelude::*;
use yew_router::prelude::*;

pub struct App;

impl Component for App {
    type Message = ();
    type Properties = ();

    fn create(_: Self::Properties, _link: ComponentLink<Self>) -> Self {
        rural::init();
        Self
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        use crate::pages::*;
        use crate::route::{Route, RouteInner};
        html! {
            <Router<Route>
                render = Router::render(|switch: Route| {
                    match (rural::get_key().is_err(), switch) {
                        (false, Route(RouteInner::HomePage)) => html! {<Home/>},
                        (true, _) | (false, Route(RouteInner::Settings)) => html! {<Settings/>},
                        (false, Route(RouteInner::Profile)) => html! {<Profile/>},
                        (false, Route(RouteInner::Debug)) => html! {<Debug/>},
                }})
            />
        }
    }
}
