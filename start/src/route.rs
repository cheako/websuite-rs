use yew_router::prelude::*;

#[derive(Switch, Debug, Clone, PartialEq)]
pub enum RouteInner {
    #[to = "/profile.html"]
    Profile,
    #[to = "/settings.html"]
    Settings,
    #[to = "/debug.html"]
    Debug,
    #[to = "/"]
    HomePage,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Route(pub RouteInner);

thread_local! {
    pub static BASE_URI: std::cell::RefCell<Option<String>> = Default::default();
}

impl Switch for Route {
    fn from_route_part<STATE>(
        mut part: std::string::String,
        state: std::option::Option<STATE>,
    ) -> (std::option::Option<Self>, std::option::Option<STATE>) {
        if let Some(index) = part.find("/websuite/") {
            let rpart = part.split_off(index + 9);
            BASE_URI.with(move |base| {
                base.borrow_mut().get_or_insert(part);
            });
            let (inner, state) = RouteInner::from_route_part(rpart, state);
            (inner.map(Route), state)
        } else {
            (None, None)
        }
    }

    fn build_route_section<STATE>(
        self,
        route: &mut std::string::String,
    ) -> std::option::Option<STATE> {
        use std::fmt::Write;
        write!(route, "{}", BASE_URI.with(|base| base.borrow().clone())?).ok()?;
        self.0.build_route_section(route)
    }
}
