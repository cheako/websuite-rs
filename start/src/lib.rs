#![feature(async_closure)]
#![recursion_limit = "2048"]
#![cfg(target_arch = "wasm32")]

mod utils;

mod app;
mod components;
mod pages;
mod route;

// use wasm_bindgen::JsCast;
// use rand::{rngs::OsRng, Rng};
use wasm_bindgen::prelude::*;
use yew::prelude::*;

// use wasm_bindgen::JsCast;
// use web_sys::{
//    CanvasRenderingContext2d, Document, HtmlCanvasElement, HtmlElement, HtmlImageElement,
//    HtmlInputElement, MouseEvent, Touch, TouchEvent, Window,
// };
// use web_sys::{Document, Window};

//use std::cell::RefCell;
// use std::rc::Rc;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen(start)]
pub fn run_main() {
    utils::set_panic_hook();

    yew::initialize();
}

#[wasm_bindgen]
pub async fn run() -> Result<(), JsValue> {
    rural::register_service_worker().await?;

    App::<app::App>::new().mount_to_body();

    Ok(())
}
