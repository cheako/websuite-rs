# Websuite-rs: Rust Websuite

## E2EE Suite built on HTML5 and PWA.

- E2EE: End to End Encryption

  Deterministic Hierarchical keys are used for signing and building symmetric encryption keys.

  - Of note choosing your root key is done by clicking `Generate` and `Apply`. This is forced at the beginning, currently. The rest of the interface doesn't make sense until a root key has been chosen.

- Suite

  Eventually there will be a selection of tools. Rich Text Notes, Shopping Lists, and Calenders immediately come to mind.

  - The current goal is a Facebook style Profile /w comments. Text support is in the post, currently only some buttons are provided as example messages.

- HTML5

  Heavy use of advanced web topics like wasm are invoked.

- PWA: Progressive Web Application

  This software is meant to be installed onto smartphones and tablets.

## Choosing a root key

Currently it's unimportant. Sometimes during testing it's necessary to test with a different key, so expect the key you choose to not be the final key used.

Eventually the Mnemonic you use should be something permanently yours. Profiles are to be children of a root key, so one root key per person(containing multiple profiles for that person) should be the norm. Support for _group_ profiles is planned.

## Style

I've no talent for communication, thus in most cases the defaults were chosen.
