#![cfg(target_arch = "wasm32")]
use idb::jtm::keys::PrivKey;
pub use idb::{
    self,
    jtm::{self, brainpool_p256r1::RIPEMD160},
};
use serde::{Deserialize, Serialize};
use shuttle::{
    get_check_sum,
    protobuf::{srv, JTMPoint, Point, U128, U160, U256},
};
use std::convert::TryFrom;
use wasm_bindgen::prelude::JsValue;

#[derive(Serialize, Deserialize, Debug)]
pub enum AnimalType {
    Cow,
    Dog,
    Turkey,
}

pub type ListenRequest = (u64, u64);

#[derive(Serialize, Deserialize, Debug)]
pub enum ListenResponse {
    Message {
        ctime: u64,
        key: RIPEMD160,
        key_sum: u64,
        seq: (u32, u32),
        time: u64,
        iv: [u8; 16],
        inner: Vec<u8>,
        check_sum: u64,
    },
    SigMessage {
        ctime: u64,
        key: RIPEMD160,
        key_sum: u64,
        time: u64,
        iv: [u8; 16],
        inner: Vec<u8>,
        check_sum: u64,
    },
}

impl From<&srv::ListenResponse> for ListenResponse {
    fn from(msg: &srv::ListenResponse) -> Self {
        let ctime = msg.get_ctime();
        if msg.has_message() {
            let msg = msg.get_message();
            let sig = msg.get_sig();
            let msg = msg.get_inner();
            let seq = msg.get_seq();
            Self::Message {
                ctime,
                key: RIPEMD160::try_from(JTMPoint::from(&Point::from(sig.get_h()))).unwrap(),
                key_sum: get_check_sum(&*U256::from(sig.get_s())),
                seq: (seq.get_num(), seq.get_of()),
                time: msg.get_time(),
                iv: *U128::from(msg.get_iv()),
                inner: msg.get_inner().to_vec(),
                check_sum: msg.get_check_sum(),
            }
        } else {
            let msg = msg.get_sig_message().get_inner();
            Self::SigMessage {
                ctime,
                key: *U160::from(msg.get_key()),
                key_sum: msg.get_key_sum(),
                time: msg.get_time(),
                iv: *U128::from(msg.get_iv()),
                inner: msg.get_inner().to_vec(),
                check_sum: msg.get_check_sum(),
            }
        }
    }
}

impl TryFrom<&JsValue> for ListenResponse {
    type Error = serde_json::error::Error;

    fn try_from(receive: &JsValue) -> Result<Self, Self::Error> {
        receive.into_serde::<Self>()
    }
}

impl TryFrom<&ListenResponse> for JsValue {
    type Error = serde_json::error::Error;

    fn try_from(send: &ListenResponse) -> Result<Self, Self::Error> {
        Self::from_serde(send)
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub enum Request {
    Start,
    // Listen(ListenRequest),
    Ding(PrivKey, Vec<ListenRequest>),
    Animal(PrivKey, AnimalType, Vec<ListenRequest>),
    Listen(Vec<ListenRequest>),
}

impl TryFrom<&JsValue> for Request {
    type Error = serde_json::error::Error;

    fn try_from(receive: &JsValue) -> Result<Self, Self::Error> {
        receive.into_serde::<Self>()
    }
}

impl TryFrom<&Request> for JsValue {
    type Error = serde_json::error::Error;

    fn try_from(send: &Request) -> Result<Self, Self::Error> {
        Self::from_serde(send)
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub enum Response {
    Listen(Vec<ListenResponse>),
    SetText(String),
}

impl TryFrom<&JsValue> for Response {
    type Error = serde_json::error::Error;

    fn try_from(receive: &JsValue) -> Result<Self, Self::Error> {
        receive.into_serde::<Self>()
    }
}

impl TryFrom<&Response> for JsValue {
    type Error = serde_json::error::Error;

    fn try_from(send: &Response) -> Result<Self, Self::Error> {
        Self::from_serde(send)
    }
}
