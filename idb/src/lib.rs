#![feature(refcell_take)]

use std::cell::{Cell, RefCell};
use std::collections::HashMap;
use std::num::NonZeroU32;
use std::ops::Deref;
use std::rc::Rc;
use std::task::{Context, Poll, Waker};

use futures::stream::Stream;

use wasm_bindgen::{closure::Closure, JsCast, JsValue};
use web_sys::{
    console::log_1, window as get_window, Event as WebEvent, IdbCursor, IdbCursorDirection,
    IdbCursorWithValue, IdbDatabase, IdbFactory, IdbIndex, IdbObjectStore, IdbOpenDbRequest,
    IdbRequest, Window,
};

type UpgradeFn = dyn FnOnce(WebEvent) -> Result<(), JsValue>;
type VersionChangeFn = dyn FnMut(WebEvent) -> Result<(), JsValue>;
type IdbOpenDbRequestEventFn = dyn FnMut();
type IdbOpenDbRequestUpdateFn = dyn FnMut(WebEvent) -> Result<(), JsValue>;

#[derive(Debug)]
struct IdbOpenDbRequestHandle {
    wakers: Vec<Rc<RefCell<Option<Waker>>>>,
    request: IdbOpenDbRequest,
    event: Closure<IdbOpenDbRequestEventFn>,
    update: Closure<IdbOpenDbRequestUpdateFn>,
}

thread_local! {
    pub static WINDOW: Window = get_window()
        .expect("should have a window in this context");
    static IDB_FACTORY: IdbFactory = WINDOW.with(|g| g.indexed_db())
        .expect("global should have indexed_db")
        .expect("indexed_db should not be None");
    pub static UPGRADE: RefCell<HashMap<String, Box<UpgradeFn>>> =
        Default::default();
    pub static VERSION_CHANGE: RefCell<HashMap<String, Box<VersionChangeFn>>> =
        Default::default();
    static IDBS: RefCell<HashMap<String, Result<Database, IdbOpenDbRequestHandle>>> = Default::default();
}

#[derive(Debug, Clone)]
pub struct Database(IdbDatabase, Rc<Closure<VersionChangeFn>>);

impl Deref for Database {
    type Target = IdbDatabase;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

pub struct OpenRequest {
    name: String,
    version: NonZeroU32,
    handler: Rc<RefCell<Option<Waker>>>,
}

pub fn open(name: String, version: NonZeroU32) -> OpenRequest {
    OpenRequest {
        name,
        version,
        handler: Default::default(),
    }
}

impl std::future::Future for OpenRequest {
    type Output = Result<Database, JsValue>;

    fn poll(self: std::pin::Pin<&mut Self>, cx: &mut Context) -> Poll<Self::Output> {
        self.handler.borrow_mut().replace(cx.waker().clone());
        IDBS.with(|idbs| {
            match idbs
                .borrow_mut()
                .entry(self.name.clone())
                .and_modify(|e| {
                    if let Err(ref mut record) = e {
                        use web_sys::IdbRequestReadyState::*;
                        match record.request.ready_state() {
                            Pending => {
                                if !record.wakers.iter().all(|e| !Rc::ptr_eq(&self.handler, e)) {
                                    record.wakers.push(self.handler.clone());
                                }
                            }
                            Done => {
                                let name = self.name.clone();
                                let onchange = Closure::wrap(Box::new(
                                    move |event: WebEvent| -> Result<(), JsValue> {
                                        #[allow(unused_unsafe)]
                                        unsafe {
                                            log_1(&"VERSION_CHANGE".into());
                                        }
                                        let e = event.clone();
                                        VERSION_CHANGE.with(|u| {
                                            if let Some(mut c) = u.borrow_mut().remove(&name) {
                                                c(e)
                                            } else {
                                                Ok(())
                                            }
                                        })?;
                                        event.target().map(|db| {
                                            db.dyn_into::<IdbDatabase>().map(|db| db.close())
                                        });
                                        Ok(())
                                    },
                                )
                                    as Box<dyn FnMut(WebEvent) -> Result<(), JsValue>>);
                                let db: IdbDatabase = record
                                    .request
                                    .result()
                                    .map(|db| db.unchecked_into())
                                    .unwrap_or_else(|_| panic!("get idb result for {}", self.name));
                                db.set_onversionchange(Some(onchange.as_ref().unchecked_ref()));
                                *e = Ok(Database(db, Rc::new(onchange)));
                            }
                            _ => panic!("unexpected ready state"),
                        };
                    }
                })
                .or_insert({
                    let name = self.name.clone();
                    let event = Closure::wrap(Box::new(move || {
                        #[allow(unused_unsafe)]
                        unsafe {
                            log_1(&"event".into());
                        }
                        IDBS.with(|idbs| {
                            idbs.borrow()
                                .get(&name)
                                .unwrap()
                                .as_ref()
                                .unwrap_err()
                                .wakers
                                .clone()
                                .into_iter()
                                .for_each(|e| {
                                    if let Some(w) = e.take() {
                                        w.wake()
                                    }
                                })
                        })
                    }) as Box<dyn FnMut()>);
                    let name = self.name.clone();
                    let update =
                        Closure::wrap(Box::new(move |e: WebEvent| -> Result<(), JsValue> {
                            #[allow(unused_unsafe)]
                            unsafe {
                                log_1(&"update".into());
                            }
                            UPGRADE.with(|u| {
                                if let Some(c) = u.borrow_mut().remove(&name) {
                                    #[allow(unused_unsafe)]
                                    unsafe {
                                        log_1(&"update:Some(_)".into());
                                    }
                                    c(e)
                                } else {
                                    #[allow(unused_unsafe)]
                                    unsafe {
                                        log_1(&"update:None".into());
                                    }
                                    Ok(())
                                }
                            })
                        })
                            as Box<dyn FnMut(WebEvent) -> Result<(), JsValue>>);
                    let request = IDB_FACTORY
                        .with(|idbf| idbf.open_with_u32(&self.name, self.version.get()))
                        .unwrap_or_else(|_| panic!("idb open {}", &self.name));
                    request.set_onsuccess(Some(event.as_ref().unchecked_ref()));
                    request.set_onerror(Some(event.as_ref().unchecked_ref()));
                    request.set_onupgradeneeded(Some(update.as_ref().unchecked_ref()));
                    Err(IdbOpenDbRequestHandle {
                        wakers: vec![self.handler.clone()],
                        request,
                        event,
                        update,
                    })
                }) {
                Ok(db) => Poll::Ready(Ok(db.clone())),
                Err(_) => Poll::Pending,
            }
        })
    }
}

pub trait HasGetRequest {
    fn get(&self, key: &JsValue) -> Result<IdbRequest, JsValue>;
    fn get_all(&self) -> Result<IdbRequest, JsValue>;
    fn get_all_with_key(&self, key: &JsValue) -> Result<IdbRequest, JsValue>;
    fn get_all_with_key_and_limit(&self, key: &JsValue, limit: u32) -> Result<IdbRequest, JsValue>;
    fn get_all_keys(&self) -> Result<IdbRequest, JsValue>;
    fn get_all_keys_with_key(&self, key: &JsValue) -> Result<IdbRequest, JsValue>;
    fn get_all_keys_with_key_and_limit(
        &self,
        key: &JsValue,
        limit: u32,
    ) -> Result<IdbRequest, JsValue>;
    fn get_key(&self, key: &JsValue) -> Result<IdbRequest, JsValue>;
}

impl HasGetRequest for IdbObjectStore {
    fn get(&self, key: &JsValue) -> Result<IdbRequest, JsValue> {
        self.get(key)
    }
    fn get_all(&self) -> Result<IdbRequest, JsValue> {
        self.get_all()
    }
    fn get_all_with_key(&self, key: &JsValue) -> Result<IdbRequest, JsValue> {
        self.get_all_with_key(key)
    }
    fn get_all_with_key_and_limit(&self, key: &JsValue, limit: u32) -> Result<IdbRequest, JsValue> {
        self.get_all_with_key_and_limit(key, limit)
    }
    fn get_all_keys(&self) -> Result<IdbRequest, JsValue> {
        self.get_all_keys()
    }
    fn get_all_keys_with_key(&self, key: &JsValue) -> Result<IdbRequest, JsValue> {
        self.get_all_keys_with_key(key)
    }
    fn get_all_keys_with_key_and_limit(
        &self,
        key: &JsValue,
        limit: u32,
    ) -> Result<IdbRequest, JsValue> {
        self.get_all_keys_with_key_and_limit(key, limit)
    }
    fn get_key(&self, key: &JsValue) -> Result<IdbRequest, JsValue> {
        self.get_key(key)
    }
}

impl HasGetRequest for IdbIndex {
    fn get(&self, key: &JsValue) -> Result<IdbRequest, JsValue> {
        self.get(key)
    }
    fn get_all(&self) -> Result<IdbRequest, JsValue> {
        self.get_all()
    }
    fn get_all_with_key(&self, key: &JsValue) -> Result<IdbRequest, JsValue> {
        self.get_all_with_key(key)
    }
    fn get_all_with_key_and_limit(&self, key: &JsValue, limit: u32) -> Result<IdbRequest, JsValue> {
        self.get_all_with_key_and_limit(key, limit)
    }
    fn get_all_keys(&self) -> Result<IdbRequest, JsValue> {
        self.get_all_keys()
    }
    fn get_all_keys_with_key(&self, key: &JsValue) -> Result<IdbRequest, JsValue> {
        self.get_all_keys_with_key(key)
    }
    fn get_all_keys_with_key_and_limit(
        &self,
        key: &JsValue,
        limit: u32,
    ) -> Result<IdbRequest, JsValue> {
        self.get_all_keys_with_key_and_limit(key, limit)
    }
    fn get_key(&self, key: &JsValue) -> Result<IdbRequest, JsValue> {
        self.get_key(key)
    }
}

type GetRequestArgs = (IdbRequest, Closure<dyn FnMut()>);

#[allow(clippy::type_complexity)]
pub struct GetRequest {
    inner: RefCell<Result<(Box<dyn HasGetRequest>, JsValue), GetRequestArgs>>,
    handler: Rc<RefCell<Option<Waker>>>,
}

pub fn get_request(os: Box<dyn HasGetRequest>, key: JsValue) -> GetRequest {
    GetRequest {
        inner: RefCell::new(Ok((os, key))),
        handler: Default::default(),
    }
}

impl std::future::Future for GetRequest {
    type Output = Result<JsValue, JsValue>;

    fn poll(self: std::pin::Pin<&mut Self>, cx: &mut Context) -> Poll<Self::Output> {
        self.handler.borrow_mut().replace(cx.waker().clone());
        let mut inner = self.inner.borrow_mut();
        if let Ok((ref os, ref key)) = *inner {
            let req = os.get(key)?;
            let h = self.handler.clone();
            let onevent = Closure::once(Box::new(move || {
                if let Some(w) = h.borrow_mut().take() {
                    w.wake();
                }
            }) as Box<dyn FnOnce()>);
            req.set_onsuccess(Some(onevent.as_ref().unchecked_ref()));
            req.set_onerror(Some(onevent.as_ref().unchecked_ref()));
            *inner = Err((req, onevent));
        }
        if let Err((ref req, _)) = *inner {
            use web_sys::IdbRequestReadyState as State;
            match req.ready_state() {
                State::Pending => Poll::Pending,
                State::Done => Poll::Ready(req.result()),
                _ => panic!("unexpected ready state"),
            }
        } else {
            unreachable!()
        }
    }
}

pub trait HasCursorAPI {
    fn open_cursor(&self) -> Result<IdbRequest, JsValue>;
    fn open_cursor_with_range(&self, range: &JsValue) -> Result<IdbRequest, JsValue>;
    fn open_cursor_with_range_and_direction(
        &self,
        range: &JsValue,
        direction: IdbCursorDirection,
    ) -> Result<IdbRequest, JsValue>;
    fn open_key_cursor(&self) -> Result<IdbRequest, JsValue>;
    fn open_key_cursor_with_range(&self, range: &JsValue) -> Result<IdbRequest, JsValue>;
    fn open_key_cursor_with_range_and_direction(
        &self,
        range: &JsValue,
        direction: IdbCursorDirection,
    ) -> Result<IdbRequest, JsValue>;
}

impl HasCursorAPI for IdbObjectStore {
    fn open_cursor(&self) -> Result<IdbRequest, JsValue> {
        self.open_cursor()
    }
    fn open_cursor_with_range(&self, range: &JsValue) -> Result<IdbRequest, JsValue> {
        self.open_cursor_with_range(range)
    }
    fn open_cursor_with_range_and_direction(
        &self,
        range: &JsValue,
        direction: IdbCursorDirection,
    ) -> Result<IdbRequest, JsValue> {
        self.open_cursor_with_range_and_direction(range, direction)
    }
    fn open_key_cursor(&self) -> Result<IdbRequest, JsValue> {
        self.open_key_cursor()
    }
    fn open_key_cursor_with_range(&self, range: &JsValue) -> Result<IdbRequest, JsValue> {
        self.open_key_cursor_with_range(range)
    }
    fn open_key_cursor_with_range_and_direction(
        &self,
        range: &JsValue,
        direction: IdbCursorDirection,
    ) -> Result<IdbRequest, JsValue> {
        self.open_key_cursor_with_range_and_direction(range, direction)
    }
}

impl HasCursorAPI for IdbIndex {
    fn open_cursor(&self) -> Result<IdbRequest, JsValue> {
        self.open_cursor()
    }
    fn open_cursor_with_range(&self, range: &JsValue) -> Result<IdbRequest, JsValue> {
        self.open_cursor_with_range(range)
    }
    fn open_cursor_with_range_and_direction(
        &self,
        range: &JsValue,
        direction: IdbCursorDirection,
    ) -> Result<IdbRequest, JsValue> {
        self.open_cursor_with_range_and_direction(range, direction)
    }
    fn open_key_cursor(&self) -> Result<IdbRequest, JsValue> {
        self.open_key_cursor()
    }
    fn open_key_cursor_with_range(&self, range: &JsValue) -> Result<IdbRequest, JsValue> {
        self.open_key_cursor_with_range(range)
    }
    fn open_key_cursor_with_range_and_direction(
        &self,
        range: &JsValue,
        direction: IdbCursorDirection,
    ) -> Result<IdbRequest, JsValue> {
        self.open_key_cursor_with_range_and_direction(range, direction)
    }
}

type CursorRequestArgs = (IdbRequest, Closure<dyn FnMut()>);
type CursorRequestVal = (JsValue, Option<IdbCursorDirection>);
pub struct CursorRequest<T: HasCursorAPI> {
    pub continue_: bool,
    pub none_on_falsy: bool,
    schedule_continue: Cell<bool>,
    inner: RefCell<Result<(T, Option<CursorRequestVal>), CursorRequestArgs>>,
    handler: Rc<RefCell<Option<Waker>>>,
}

pub fn open_key_cursor<T: HasCursorAPI>(
    os: T,
    params: Option<(JsValue, Option<IdbCursorDirection>)>,
) -> CursorRequest<T> {
    CursorRequest {
        continue_: true,
        none_on_falsy: true,
        schedule_continue: Default::default(),
        inner: RefCell::new(Ok((os, params))),
        handler: Default::default(),
    }
}

impl<T: HasCursorAPI> Stream for CursorRequest<T> {
    type Item = Result<(JsValue, IdbCursor), JsValue>;

    fn poll_next(self: std::pin::Pin<&mut Self>, cx: &mut Context) -> Poll<Option<Self::Item>> {
        let mut inner = self.inner.borrow_mut();
        if let Ok((ref os, ref query)) = *inner {
            let req = match *query {
                None => os.open_key_cursor()?,
                Some((ref range, None)) => os.open_key_cursor_with_range(range)?,
                Some((ref range, Some(direction))) => {
                    os.open_key_cursor_with_range_and_direction(range, direction)?
                }
            };
            let h = self.handler.clone();
            let onevent = Closure::wrap(Box::new(move || {
                /* if let Some(w) = h.borrow_mut().take() {
                    w.wake();
                } */
                // This should fail if None...
                h.borrow_mut()
                    .take()
                    .expect("CursorRequest: onevent called with no waker")
                    .wake();
            }) as Box<dyn FnMut()>);
            req.set_onsuccess(Some(onevent.as_ref().unchecked_ref()));
            req.set_onerror(Some(onevent.as_ref().unchecked_ref()));
            *inner = Err((req, onevent));
        }
        if let Err((ref req, _)) = *inner {
            use web_sys::IdbRequestReadyState as State;
            if self.schedule_continue.get() {
                req.result()
                    .unwrap()
                    .unchecked_into::<IdbCursor>()
                    .continue_()?;
                self.schedule_continue.set(false);
            }
            match req.ready_state() {
                State::Pending => {
                    self.handler.borrow_mut().replace(cx.waker().clone());
                    Poll::Pending
                }
                State::Done => {
                    /* Be mighty confident? */
                    let cursor: IdbCursor = req.result().unwrap().unchecked_into();
                    let key = cursor.key()?;
                    if self.continue_ {
                        cursor.continue_()?;
                    }
                    Poll::Ready(match (self.none_on_falsy, key.is_truthy()) {
                        (false, _) | (true, true) => {
                            if self.continue_ {
                                self.schedule_continue.set(true);
                            }
                            Some(Ok((key, cursor)))
                        }
                        (true, false) => None,
                    })
                }
                _ => panic!("unexpected ready state"),
            }
        } else {
            unreachable!()
        }
    }
}

type CursorWithValueRequestArgs = (IdbRequest, Closure<dyn FnMut()>);
type CursorWithValueRequestVal = (JsValue, Option<IdbCursorDirection>);
pub struct CursorWithValueRequest<T: HasCursorAPI> {
    pub continue_: bool,
    pub none_on_falsy: bool,
    schedule_continue: Cell<bool>,
    inner: RefCell<Result<(T, Option<CursorWithValueRequestVal>), CursorWithValueRequestArgs>>,
    handler: Rc<RefCell<Option<Waker>>>,
}

pub fn open_cursor<T: HasCursorAPI>(
    os: T,
    params: Option<(JsValue, Option<IdbCursorDirection>)>,
) -> CursorWithValueRequest<T> {
    CursorWithValueRequest {
        continue_: true,
        none_on_falsy: true,
        schedule_continue: Default::default(),
        inner: RefCell::new(Ok((os, params))),
        handler: Default::default(),
    }
}

impl<T: HasCursorAPI> Stream for CursorWithValueRequest<T> {
    type Item = Result<((JsValue, JsValue), IdbCursorWithValue), JsValue>;

    fn poll_next(self: std::pin::Pin<&mut Self>, cx: &mut Context) -> Poll<Option<Self::Item>> {
        let mut inner = self.inner.borrow_mut();
        if let Ok((ref os, ref query)) = *inner {
            let req = match *query {
                None => os.open_cursor()?,
                Some((ref range, None)) => os.open_cursor_with_range(range)?,
                Some((ref range, Some(direction))) => {
                    os.open_cursor_with_range_and_direction(range, direction)?
                }
            };
            let h = self.handler.clone();
            let onevent = Closure::once(Box::new(move || {
                /* if let Some(w) = h.borrow_mut().take() {
                    w.wake();
                } */
                // This should fail if None...
                h.borrow_mut()
                    .take()
                    .expect("CursorWithValueRequest: onevent called with no waker")
                    .wake();
            }) as Box<dyn FnOnce()>);
            req.set_onsuccess(Some(onevent.as_ref().unchecked_ref()));
            req.set_onerror(Some(onevent.as_ref().unchecked_ref()));
            *inner = Err((req, onevent));
        }
        if let Err((ref req, _)) = *inner {
            use web_sys::IdbRequestReadyState as State;
            if self.schedule_continue.get() {
                req.result()
                    .unwrap()
                    .unchecked_into::<IdbCursorWithValue>()
                    .continue_()?;
                self.schedule_continue.set(false);
            }
            match req.ready_state() {
                State::Pending => {
                    self.handler.borrow_mut().replace(cx.waker().clone());
                    Poll::Pending
                }
                State::Done => {
                    /* Be mighty confident? */
                    let cursor: IdbCursorWithValue = req.result().unwrap().unchecked_into();
                    let data = (cursor.key()?, cursor.value()?);
                    Poll::Ready(match (self.none_on_falsy, data.0.is_truthy()) {
                        (false, _) | (true, true) => {
                            if self.continue_ {
                                self.schedule_continue.set(true);
                            }
                            Some(Ok((data, cursor)))
                        }
                        (true, false) => None,
                    })
                }
                _ => panic!("unexpected ready state"),
            }
        } else {
            unreachable!()
        }
    }
}
