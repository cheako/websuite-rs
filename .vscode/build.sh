#!/bin/sh
(
    cd start
    wasm-pack build --dev --target web --out-name start --out-dir ../public/websuite
) 2>&1 | sponge >&2 &
(
    cd urban
    wasm-pack build --dev --target no-modules --out-name urban --out-dir ../public/websuite
) 2>&1 | sponge >&2 &
cargo build --bins 2>&1 | sponge >&2
wait
wait
