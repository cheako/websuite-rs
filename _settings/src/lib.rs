#![feature(async_closure)]

// mod mnemonic;
mod utils;

// use std::cell::RefCell;
// use std::rc::Rc;

use wasm_bindgen::JsCast;
// use js_sys::Array;
use wasm_bindgen::prelude::*;
// use wasm_bindgen::JsCast;
//    HtmlElement,
use web_sys::{Document, HtmlButtonElement, HtmlInputElement, HtmlTextAreaElement, Window};

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

use rural::DOCUMENT;

thread_local! {
    static PHRASE: (HtmlTextAreaElement, HtmlTextAreaElement) = (DOCUMENT.with(|d|d.get_element_by_id("phrase"))
        .expect("Document should have id 'phrase'").dyn_into()
        .expect("phrase to be HtmlTextAreaElement"), DOCUMENT.with(|d|d.get_element_by_id("passphrase"))
        .expect("Document should have id 'passphrase'").dyn_into()
        .expect("passphrase to be HtmlTextAreaElement"));
}

// Called by our JS entry point to run the example
#[wasm_bindgen]
pub async fn run() -> Result<(), JsValue> {
    utils::set_panic_hook();

    // rural::configure_header();
    wasm_bindgen_futures::JsFuture::from(rural::register_service_worker().await?).await?;

    let apply_callback = Closure::wrap(Box::new(move || {
        thread_local! {
            static D: HtmlInputElement = DOCUMENT.with(|d|d.get_element_by_id("d"))
                .expect("Document should have id 'd'").dyn_into()
                .expect("d to be HtmlInputElement");
            static CHAINCODE: HtmlInputElement = DOCUMENT.with(|d|d.get_element_by_id("chain-code"))
                .expect("Document should have id 'chain-code'").dyn_into()
                .expect("chain-code to be HtmlInputElement");
        }

        use num_traits::Num;
        rural::STORAGE
            .with(|s| {
                s.set_item(
                    "key_serde",
                    &serde_json::to_string(&PrivKeyStorage {
                        phrase: PHRASE.with(|(p, _)| p.value()),
                        passphrase: PHRASE.with(|(_, p)| p.value()),
                        d: num_bigint_dig::BigUint::from_str_radix(&D.with(|d| d.value()), 36)
                            .expect("JS to supply valid d number"),
                        chain_code: base64::decode(&CHAINCODE.with(|cc| cc.value()))
                            .expect("chain-code to contain base64")[0..32]
                            .to_vec(),
                    })
                    .expect("serde_json<PrivKey>::to_string()"),
                )
            })
            .expect("set_item key_json");
    }) as Box<dyn FnMut()>);
    DOCUMENT
        .with(|d| d.get_element_by_id("apply"))
        .expect("Document should have id 'apply'")
        .dyn_into::<HtmlButtonElement>()
        .expect("apply to be HtmlButtonElement")
        .set_onclick(Some(apply_callback.as_ref().unchecked_ref()));
    // forget the callback to keep it alive
    apply_callback.forget();

    if let Some(Ok(pkey)) = rural::STORAGE
        .with(|s| s.get_item("key_serde"))
        .expect("get_item key_json")
        .map(|ref json| serde_json::from_str::<PrivKey>(json))
    {
        PHRASE.with(|(phrase, passphrase)| {
            phrase.set_value(&pkey.phrase);
            passphrase.set_value(&pkey.passphrase);
            phrase
                .dispatch_event(&web_sys::CustomEvent::new("change").unwrap())
                .unwrap();
        });
    }

    Ok(())
}
