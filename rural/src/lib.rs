#![feature(async_closure)]
#![allow(unused_imports)]
#![cfg(target_arch = "wasm32")]
use js_sys::Promise;
use wasm_bindgen::{prelude::Closure, JsCast, JsValue};
use web_sys::{Document, MessageEvent, ServiceWorkerContainer, ServiceWorkerRegistration, Storage};

use num_bigint_dig::BigUint;

pub use bs58;
pub use commute::{
    self,
    idb::{self, WINDOW},
    jtm::{
        self,
        keys::{ChainCode, PrivKey, PrivKeyBytes, PubKey, PubKeyBytes},
    },
};

thread_local! {
    static SERVICE_WORKER_CONTAINER: ServiceWorkerContainer =
        WINDOW.with(|w|w.navigator().service_worker());
    pub static STORAGE: Storage = WINDOW.with(|w|w.local_storage())
        .expect("window should have local storage")
        .expect("local storage should exist");
    pub static SERVICE_WORKER_REGISTARTION:
        std::cell::RefCell<Option<ServiceWorkerRegistration>> =
            Default::default();
    pub static DOCUMENT: Document = WINDOW.with(|w|w.document())
        .expect("window should have a document");
}

/*
thread_local! {
    static PROFILE_ID: HtmlSelectElement = DOCUMENT.with(|d|d
            .get_element_by_id("profile_id"))
        .expect("document should have a profile_id")
        .dyn_into::<HtmlSelectElement>()
        .expect("#profile_id be an `HtmlSelectElement`");
}
 */

#[derive(Clone, Debug, serde_derive::Deserialize, serde_derive::Serialize)]
pub struct PrivKeyStorage {
    pub phrase: String,
    pub passphrase: String,
    pub d: String,
}

pub fn init() {
    idb::init();
}

pub async fn register_service_worker() -> Result<JsValue, JsValue> {
    let ret = wasm_bindgen_futures::JsFuture::from(SERVICE_WORKER_CONTAINER.with(|swc| {
        let message = Closure::wrap(Box::new(move |e: MessageEvent| {
            #[allow(unused_unsafe)]
            unsafe {
                web_sys::console::log_2(&"register_service_worker::message()".into(), &e);
            }
        }) as Box<dyn FnMut(MessageEvent)>);
        swc.set_onmessage(Some(message.as_ref().unchecked_ref()));
        message.forget();
        swc.register("sw.js")
    }))
    .await;
    send_message(commute::Request::Start).await?;
    ret
}
/*
pub fn configure_header() -> Result<(), JsValue> {
    let cb = Closure::wrap(Box::new(|| -> Result<(), JsValue> {
        PROFILE_ID.with(move |p| -> Result<(), JsValue> {
            if p.value() == "New..." {
                use std::convert::TryInto;
                let option = HtmlOptionElement::new()?;
                option.set_text(&format!("{}", p.length()));
                p.add_with_html_option_element_and_opt_i32(
                    &option,
                    Some(
                        (p.length() - 1)
                            .try_into()
                            .map_err(|e| Into::<JsValue>::into(format!("u32 -> i32: {}", e)))?,
                    ),
                )
            } else {
                Ok(())
            }
        })
    }) as Box<dyn FnMut() -> Result<(), JsValue>>);
    PROFILE_ID.with(move |p| -> Result<(), JsValue> {
        p.set_onchange(Some(cb.as_ref().unchecked_ref()));
        cb.forget();
        if p.length() == 1 {
            use std::convert::TryInto;
            let option = HtmlOptionElement::new()?;
            option.set_text(&format!("{}", p.length()));
            p.add_with_html_option_element_and_opt_i32(
                &option,
                Some(
                    (p.length() - 1)
                        .try_into()
                        .map_err(|e| Into::<JsValue>::into(format!("u32 -> i32: {}", e)))?,
                ),
            )
        } else {
            Ok(())
        }
    })
}
 */
pub fn get_key() -> Result<PrivKey, JsValue> {
    match serde_json::from_str::<PrivKeyStorage>(
        &STORAGE
            .with(|s| s.get_item("key_serde"))?
            .ok_or_else(|| -> JsValue { "key_serde not found".into() })?,
    ) {
        Ok(p) => {
            let mut ret = [0; 73];
            bs58::decode(p.d)
                .into(&mut ret[73 - 64..73])
                .map_err(|e| JsValue::from(format!("bs58: {}", e)))?;
            Ok((&ret).into())
        }
        Err(e) => Err(format!("serde_json: {}", e).into()),
    }
}

/*
pub fn get_profile_id() -> i32 {
    /* ToDo: allow removing profiles */
    PROFILE_ID.with(|e| e.selected_index())
}
 */
pub async fn send_message(request: commute::Request) -> Result<JsValue, JsValue> {
    let swr = wasm_bindgen_futures::JsFuture::from(
        SERVICE_WORKER_CONTAINER.with(|swc| swc.ready()).unwrap(),
    )
    .await
    .unwrap()
    .dyn_into::<ServiceWorkerRegistration>()
    .unwrap();
    if let Some(sw) = swr.active() {
        use std::convert::TryInto;
        sw.post_message(&(&request).try_into().unwrap())
    } else {
        Err("Expected active sw".into())
    }
    .unwrap();
    Ok(JsValue::UNDEFINED)
}

pub fn send_message_promise(request: commute::Request) -> Promise {
    wasm_bindgen_futures::future_to_promise(send_message(request))
}
