pub use jtm;
use jtm::{brainpool_p256r1::ecdsa_verify, num_bigint::BigUint, Digest, Sha256, ShaOutput};
pub use protobuf::{
    self,
    srv::{
        InsertMessage as BinInsertMsg, ListenRequest as BinListenRequest,
        ListenResponse as BinListenResponse, Request as BinRequest, Response as BinResponse,
        Sig as BinSig, SigInsertMessage as BinSigInsertMessage,
    },
    Message, Sig, U256,
};

fn get_insert_z(insert: &BinInsertMsg) -> ShaOutput<Sha256> {
    Sha256::new()
        .chain(b"websuite_ecdsa_begin")
        .chain(&insert.write_to_bytes().unwrap())
        .chain(b"websuite_ecdsa_end")
        .finalize()
}

pub fn test_sign(sig: &BinSig, insert: &BinInsertMsg) -> bool {
    let sig = Sig::from(sig);
    ecdsa_verify(
        sig.h,
        BigUint::from_bytes_be(&get_insert_z(insert)),
        sig.r,
        sig.s,
    )
}

#[allow(dead_code)]
fn get_sig_insert_z(insert: &ShaOutput<Sha256>, sig_insert: u64) -> BigUint {   
    BigUint::from_bytes_be(  
        &Sha256::new()
            .chain(b"websuite_ecdsa_begin")
            .chain(insert)
            .chain(b"websuite_ecdsa_continue")
            .chain(&sig_insert.to_be_bytes())
            .chain(b"websuite_ecdsa_end")
            .finalize(),
    )
} 

pub fn sig_test_sign(sig: &BinSig, insert: &BinSigInsertMsg) -> bool {
    /* let sig = Sig::from(sig);
    ecdsa_verify(
        sig.h,
        BigUint::from_bytes_be(&get_sig_insert_z(insert)),
        sig.r,
        sig.s,
    ); */
    true
}

#[cfg(target_arch = "wasm32")]
pub use wasm::*;
#[cfg(target_arch = "wasm32")]
mod wasm {
    use block_modes::block_padding::Pkcs7;
    use block_modes::{BlockMode, Cbc};
    pub use commute::{
        ListenRequest as JSONListenRequest, Request as JSONRequest, Response as JSONResponse,
    };
    use generic_array::GenericArray;
    use jtm::{
        brainpool_p256r1::ecdsa_sign, keys::*, num_bigint::BigUint, sha2::digest, Digest, Sha256,
    };
    use protobuf::{
        self,
        srv::{
            InsertMessage as BinInsertMsg, ListenRequest as BinListenRequest,
            Request as BinRequest, Sig as BinSig,
        },
        Message,
    };
    use twofish::Twofish;

    type Sha256Output = digest::Output<Sha256>;
    type TwofishCbc = Cbc<Twofish, Pkcs7>;

    fn get_inner_from_encrypted(
        pubkey: &PubKey,
        iv: &GenericArray<u8, <TwofishCbc as BlockMode<Twofish, Pkcs7>>::IvSize>,
        encrypted: &[u8],
    ) -> Vec<u8> {
        let twofish = get_twofish(pubkey);
        let engine = TwofishCbc::new_fix(&twofish, iv);
        engine.encrypt_vec(encrypted)
    }

    fn get_twofish(pubkey: &PubKey) -> Sha256Output {
        let mut ck = Sha256::default();
        ck.update(b"websuite_twofish_begin");
        /*
          Prof of parent key.  No way with just the Point from the signature can
          both of these be derived, where the chain_code could be found pared in
          a 4.6 Serialization format...  ?? Somewhere ??
        */
        ck.update(&pubkey.one_left.unwrap());
        ck.update(&pubkey.chain_code);
        ck.update(b"websuite_twofish_end");
        ck.finalize()
    }

    pub fn get_checksum(input: &[u8]) -> u64 {
        use std::hash::Hasher;
        let mut ck = std::hash::SipHasher::new();
        ck.update(input);
        ck.finish()
    }

    fn get_sign(insert: &mut BinInsertMsg, privkey: &mut PrivKey) -> BinSig {
        use rand::Rng;
        let (r, s) = ecdsa_sign(super::get_z(insert), privkey.d.clone(), |x| {
            rand::thread_rng().gen_range(BigUint::default(), x)
        });
        #[allow(unused_unsafe)]
        (&protobuf::Sig {
            h: privkey.get_h().clone(),
            r,
            s,
        })
            .into()
    }

    fn encrypted_to_request(
        privkey: &mut PrivKey,
        encrypted: &protobuf::rural::Encrypted,
        x: &[JSONListenRequest],
    ) -> BinRequest {
        use protobuf::{protobuf::RepeatedField, srv::*};
        use rand::RngCore;
        let encrypted = encrypted.write_to_bytes().unwrap();
        let ck = get_checksum(&encrypted);
        let mut iv: GenericArray<u8, <TwofishCbc as BlockMode<Twofish, Pkcs7>>::IvSize> =
            Default::default();
        rand::thread_rng().fill_bytes(iv.as_mut_slice());
        let inner = get_inner_from_encrypted(&privkey.into(), &iv, &encrypted);
        let mut imc = InsertMessageCommon::new();
        imc.set_magic(0x236708d2);
        imc.set_inner(inner);
        imc.set_check_sum(ck);
        let mut im = InsertMessage::new();
        im.set_inner(imc);
        let mut i = Insert::new();
        i.set_sig(get_sign(&mut im, privkey));
        i.set_inner(im);
        let mut ret = BinRequest::new();
        ret.set_insert(RepeatedField::from_vec(vec![i]));
        ret.set_listen(RepeatedField::from_vec(
            x.iter()
                .map(|(x, y)| {
                    let mut r = BinListenRequest::new();
                    r.set_min_ctime(*x);
                    r.set_listen_on_key(*y);
                    r
                })
                .collect(),
        ));
        ret
    }

    pub fn build_request(req: &mut JSONRequest) -> BinRequest {
        use protobuf::rural::{Test_Animal::*, *};
        use JSONRequest::*;
        match req {
            Start => unimplemented!(),
            Ding(privkey, x) => {
                let mut test = Test::new();
                test.set_animal(DING);
                let mut encrypted = Encrypted::new();
                encrypted.set_test(test);
                encrypted_to_request(privkey, &encrypted, x)
            }
            Animal(ref mut privkey, ref a, x) => {
                use commute::AnimalType::*;
                let _twofish = get_twofish(&privkey.into());
                let mut test = Test::new();
                test.set_animal(match a {
                    Cow => COW,
                    Dog => DOG,
                    Turkey => TURKEY,
                });
                let mut encrypted = Encrypted::new();
                encrypted.set_test(test);
                encrypted_to_request(privkey, &encrypted, x)
            }
            Listen(x) => {
                let mut r = BinRequest::new();
                r.set_listen(protobuf::protobuf::RepeatedField::from_vec(
                    x.iter()
                        .map(|(x, y)| {
                            let mut r = BinListenRequest::new();
                            r.set_min_ctime(*x);
                            r.set_listen_on_key(*y);
                            r
                        })
                        .collect(),
                ));
                r
            }
        }
    }
}
